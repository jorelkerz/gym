USE [Gym]
GO

/****** Object:  Table [dbo].[RutinaCliente]    Script Date: 29/03/2016 12:48:02 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[RutinaCliente](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idCliente] [int] NOT NULL,
	[dia] [nvarchar](50) NULL,
	[idHipertrofia] [int] NULL,
	[idCardio] [int] NULL,
	[estado] [nvarchar](100) NULL,
 CONSTRAINT [PK_RutinaCliente] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[idCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[RutinaCliente]  WITH CHECK ADD  CONSTRAINT [FK_RutinaCliente_Cardio] FOREIGN KEY([idCardio])
REFERENCES [dbo].[Cardio] ([id])
GO

ALTER TABLE [dbo].[RutinaCliente] CHECK CONSTRAINT [FK_RutinaCliente_Cardio]
GO

ALTER TABLE [dbo].[RutinaCliente]  WITH CHECK ADD  CONSTRAINT [FK_RutinaCliente_Cliente] FOREIGN KEY([idCliente])
REFERENCES [dbo].[Cliente] ([id])
GO

ALTER TABLE [dbo].[RutinaCliente] CHECK CONSTRAINT [FK_RutinaCliente_Cliente]
GO

ALTER TABLE [dbo].[RutinaCliente]  WITH CHECK ADD  CONSTRAINT [FK_RutinaCliente_Hipertrofia] FOREIGN KEY([idHipertrofia])
REFERENCES [dbo].[Hipertrofia] ([id])
GO

ALTER TABLE [dbo].[RutinaCliente] CHECK CONSTRAINT [FK_RutinaCliente_Hipertrofia]
GO



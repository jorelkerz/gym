USE [Gym]
GO

/****** Object:  Table [dbo].[Medicion]    Script Date: 29/03/2016 12:47:32 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Medicion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[idCliente] [int] NOT NULL,
	[fechaRegistro] [datetime] NULL,
	[peso] [decimal](18, 0) NULL,
	[ritmoCardiaco] [decimal](18, 0) NULL,
	[brazoRelajado] [decimal](18, 0) NULL,
	[brazoTension] [decimal](18, 0) NULL,
	[antebrazo] [decimal](18, 0) NULL,
	[muslo] [decimal](18, 0) NULL,
	[pechoNomal] [decimal](18, 0) NULL,
	[pechoHinchado] [decimal](18, 0) NULL,
	[gemelos] [decimal](18, 0) NULL,
	[cintura] [decimal](18, 0) NULL,
	[cadera] [decimal](18, 0) NULL,
	[porcentajeGrasa] [decimal](18, 0) NULL,
	[IMC] [decimal](18, 0) NULL,
 CONSTRAINT [PK_Medicion] PRIMARY KEY CLUSTERED 
(
	[id] ASC,
	[idCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Medicion] ADD  DEFAULT (getdate()) FOR [fechaRegistro]
GO

ALTER TABLE [dbo].[Medicion]  WITH CHECK ADD  CONSTRAINT [FK_Medicion_Cliente] FOREIGN KEY([idCliente])
REFERENCES [dbo].[Cliente] ([id])
GO

ALTER TABLE [dbo].[Medicion] CHECK CONSTRAINT [FK_Medicion_Cliente]
GO



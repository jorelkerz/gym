set dateformat dmy;
--Procedimientos almacenados de cliente

 CREATE PROCEDURE usp_INSERT_Cliente
	(@id int,
	@nombre nvarchar(50),
	@fechaNacimiento date,
	@genero nvarchar(50),
	@telefono nvarchar(50),
	@email nvarchar(50),
	@descripcionDieta nvarchar(250)
	)
AS 
	insert into Cliente(id ,nombre ,fechaNacimiento  ,genero, telefono ,email ,descripcionDieta,estado ) 
	 values(@id ,@nombre ,@fechaNacimiento ,@genero, @telefono , @email, @descripcionDieta,'1' )
;
USE [Gym]
GO

 CREATE PROCEDURE usp_SELECT_Cliente_All
	AS 
	SELECT [id]
      ,[nombre]
      ,[fechaNacimiento]
      ,[fechaRegistro]
      ,[genero]
      ,[telefono]
      ,[email]
      ,[descripcionDieta]
  FROM [dbo].[Cliente]
 where estado = '1'


 CREATE PROCEDURE usp_DELETE_Cliente_ByID
 (@id int)
 AS
 Delete from cliente where id = @id

  CREATE PROCEDURE usp_UPDATE_Cliente
	(@id int,
	@nombre nvarchar(50),
	@fechaNacimiento date,
	@genero nvarchar(50),
	@telefono nvarchar(50),
	@email nvarchar(50),
	@descripcionDieta nvarchar(250)
	)
AS 
	update Cliente set nombre = @nombre,fechaNacimiento = @fechaNacimiento,genero = @genero, telefono = @telefono,
	email = @email, descripcionDieta = @descripcionDieta where id = @id
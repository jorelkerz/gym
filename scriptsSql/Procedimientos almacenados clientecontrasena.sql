USE [Gym]
GO
/****** Object:  StoredProcedure [dbo].[usp_UPDATE_Cliente]    Script Date: 13/04/2016 11:48:31 a.m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 Create PROCEDURE [dbo].[usp_UPDATE_ClienteClave]
	(@id int,
	@contrasena nvarchar(50)
)
AS 
	update Cliente set contrasena = @contrasena where id = @id
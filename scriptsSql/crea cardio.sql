USE [Gym]
GO

/****** Object:  Table [dbo].[Cardio]    Script Date: 29/03/2016 12:45:29 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Cardio](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [nvarchar](100) NULL,
	[url] [nvarchar](500) NULL,
	[tiempo] [nvarchar](100) NULL,
	[descanso] [nvarchar](100) NULL,
	[calorias] [nvarchar](100) NULL,
	[estado] [nvarchar](100) NULL,
 CONSTRAINT [PK_Cardio] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO



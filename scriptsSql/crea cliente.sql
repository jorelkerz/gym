USE [Gym]
GO

/****** Object:  Table [dbo].[Cliente]    Script Date: 29/03/2016 12:46:14 p.m. ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Cliente](
	[id] [int] NOT NULL,
	[nombre] [nvarchar](50) NULL,
	[fechaNacimiento] [date] NULL,
	[fechaRegistro] [datetime] NULL,
	[genero] [nvarchar](50) NULL,
	[contrasena] [nvarchar](50) NULL,
	[telefono] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[estado] [nvarchar](50) NULL,
	[descripcionDieta] [nvarchar](250) NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[Cliente] ADD  DEFAULT (getdate()) FOR [fechaRegistro]
GO

/*
ALTER TABLE [dbo].[Cliente]  WITH CHECK ADD  CONSTRAINT [FK_Cliente_Dieta] FOREIGN KEY([idDieta])
REFERENCES [dbo].[Dieta] ([id])
GO

ALTER TABLE [dbo].[Cliente] CHECK CONSTRAINT [FK_Cliente_Dieta]
GO

ALTER TABLE [dbo].[Cliente] DROP CONSTRAINT [FK_Cliente_Dieta] 
GO
*/
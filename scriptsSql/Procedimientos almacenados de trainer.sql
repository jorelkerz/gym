/****** Script para el comando SelectTopNRows de SSMS  ******/
CREATE PROCEDURE usp_SELECT_Trainer_All
as
SELECT        Cliente.id, Cliente.nombre, RutinaCliente.dia, RutinaCliente.idHipertrofia, RutinaCliente.idCardio, Hipertrofia.nombre AS nombreH, Cardio.nombre AS nombreC
FROM            Cliente INNER JOIN
						 RutinaCliente ON Cliente.id = RutinaCliente.idCliente INNER JOIN
						 Hipertrofia ON RutinaCliente.idHipertrofia = Hipertrofia.id INNER JOIN
						 Cardio ON RutinaCliente.idCardio = Cardio.id
order by Cliente.id ASC, RutinaCliente.dia ASC


CREATE PROCEDURE usp_INSERT_Trainer(
@idCliente int,
@dia nvarchar(50),
@idHipertrofia int,
@idCardio int)
as
INSERT INTO [dbo].[RutinaCliente] ([idCliente],[dia],[idHipertrofia],[idCardio],[estado])
     VALUES (@idCliente,@dia,@idHipertrofia,@idCardio,'1')





CREATE PROCEDURE usp_DELETE_Trainer(
@idCliente int,
@dia nvarchar(50),
@idHipertrofia int,
@idCardio int)
as
DELETE FROM [dbo].[RutinaCliente]
      WHERE idCliente = @idCliente and dia = @dia and idHipertrofia = @idHipertrofia and idCardio = @idCardio



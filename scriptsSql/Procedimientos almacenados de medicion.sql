set dateformat dmy;
--Procedimientos almacenados de Medicion

alter PROCEDURE usp_SELECT_Medicion_ByIDCliente
	(@idCliente int)
AS 
	SELECT top 1 [id]
      ,[idCliente]
      ,[fechaRegistro]
      ,[peso]
      ,[ritmoCardiaco]
      ,[brazoRelajado]
      ,[brazoTension]
      ,[antebrazo]
      ,[muslo]
      ,[pechoNomal]
      ,[pechoHinchado]
      ,[gemelos]
      ,[cintura]
      ,[cadera]
      ,[porcentajeGrasa]
      ,[IMC]
  FROM [dbo].[Medicion]
	Where (idCliente =  @idCliente)  order by fechaRegistro desc


	create Procedure usp_DELETE_Medicion_ByIDCliente(@idCliente int)
	as
	delete from Medicion where @idCliente = idCliente


	USE [Gym]
GO


create Procedure usp_INSERT_Medicion_ByIDCliente(
@idCliente int,
@peso decimal,
@ritmoCardiaco decimal,
@brazoRelajado decimal,
@brazoTension decimal,
@antebrazo decimal,
@muslo decimal,
@pechoNomal decimal,
@pechoHinchado decimal,
@gemelos decimal,
@cintura decimal,
@cadera decimal,
@porcentajeGrasa decimal,
@IMC decimal)
as
INSERT INTO [dbo].[Medicion] ([idCliente],[peso],[ritmoCardiaco],[brazoRelajado],[brazoTension],[antebrazo],[muslo],[pechoNomal],[pechoHinchado],[gemelos],[cintura],[cadera],[porcentajeGrasa],[IMC])
     VALUES (@idCliente,@peso,@ritmoCardiaco,@brazoRelajado,@brazoTension,@antebrazo,@muslo,@pechoNomal,@pechoHinchado,@gemelos,@cintura,@cadera,@porcentajeGrasa, @IMC)
GO


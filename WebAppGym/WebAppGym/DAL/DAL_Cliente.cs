﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebAppGym.Clases;
using WebAppGym.Persistencia;

namespace WebAppGym.DAL
{
    public class DAL_Cliente
    {
        // Create
        public static void Insertar(Cliente cliente)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("usp_INSERT_Cliente");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@id", cliente.id);
            comando.Parameters.AddWithValue("@contrasena", cliente.contrasena);
            comando.Parameters.AddWithValue("@nombre", cliente.nombre);
            comando.Parameters.AddWithValue("@fechaNacimiento", cliente.fechaNacimiento);
            comando.Parameters.AddWithValue("@genero", cliente.genero);
            comando.Parameters.AddWithValue("@telefono", cliente.telefono);
            comando.Parameters.AddWithValue("@email", cliente.email);
            comando.Parameters.AddWithValue("@descripcionDieta", cliente.descripcionDieta);


            db.ExecuteNonQuery(comando);
        }

        // Read
        public static DataSet SeleccionarTodos()
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("usp_SELECT_Cliente_All");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;

            DataSet ds = db.ExecuteReader(comando, "Cliente");
            return ds;
        }



        // Read
        public static DataSet SeleccionarClientePorID(int id)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");
          

            SqlCommand comando = new SqlCommand("usp_SELECT_Cliente_ByID");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@id",id);

            DataSet ds = db.ExecuteReader(comando,"Cliente");
            return ds;
        }


        // Create
        public static void Modificar(Cliente cliente)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("usp_UPDATE_Cliente");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@id", cliente.id);
            comando.Parameters.AddWithValue("@nombre", cliente.nombre);
            comando.Parameters.AddWithValue("@fechaNacimiento", cliente.fechaNacimiento);
            comando.Parameters.AddWithValue("@genero", cliente.genero);
            //comando.Parameters.AddWithValue("@contrasena", cliente.nombre);
            comando.Parameters.AddWithValue("@telefono", cliente.telefono);
            comando.Parameters.AddWithValue("@email", cliente.email);
            comando.Parameters.AddWithValue("@descripcionDieta", cliente.descripcionDieta);


            db.ExecuteNonQuery(comando);
        }





        // Create
        public static void Eliminar(int id)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("usp_DELETE_Cliente_ByID");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@id", id);

            db.ExecuteNonQuery(comando);
        }



        public static void ModificarClienteContrasena(int id, String contrasena)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("usp_UPDATE_ClienteClave");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@id", id);
            comando.Parameters.AddWithValue("@contrasena", contrasena);

            db.ExecuteNonQuery(comando);
        }



    }//fin class
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebAppGym.Clases;
using WebAppGym.Persistencia;

namespace WebAppGym.DAL
{
    public class DAL_Medicion
    {
        // Create
        public static void Insertar(Medicion medicion)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("usp_INSERT_Medicion_ByIDCliente");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@idCliente", medicion.id);
            comando.Parameters.AddWithValue("@peso", medicion.peso);
            comando.Parameters.AddWithValue("@ritmoCardiaco", medicion.ritmoCardiaco);
            comando.Parameters.AddWithValue("@brazoRelajado", medicion.brazoRelajado);
            comando.Parameters.AddWithValue("@brazoTension", medicion.brazoTension);
            comando.Parameters.AddWithValue("@antebrazo", medicion.antebrazo);
            comando.Parameters.AddWithValue("@muslo", medicion.muslo);
            comando.Parameters.AddWithValue("@pechoNomal", medicion.pechoNomal);
            comando.Parameters.AddWithValue("@pechoHinchado", medicion.pechoHinchado);
            comando.Parameters.AddWithValue("@gemelos", medicion.gemelos);
            comando.Parameters.AddWithValue("@cintura", medicion.cintura);
            comando.Parameters.AddWithValue("@cadera", medicion.cadera);
            comando.Parameters.AddWithValue("@porcentajeGrasa", medicion.porcentajeGrasa);
            comando.Parameters.AddWithValue("@altura", medicion.altura);
            comando.Parameters.AddWithValue("@IMC", medicion.IMC);
 
            db.ExecuteNonQuery(comando);
        }

        // Read
        public static DataSet SeleccionarTodos()
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("usp_SELECT_Cliente_All");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;

            DataSet ds = db.ExecuteReader(comando, "Medicion");
            return ds;
        }



        // Read
        public static DataSet SeleccionarMedicionPorIDCliente(int idCliente)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");


            SqlCommand comando = new SqlCommand("usp_SELECT_Medicion_ByIDCliente");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@idCliente", idCliente);

            DataSet ds = db.ExecuteReader(comando, "Medicion");
            return ds;
        }

        public static DataSet SeleccionarMedicionReporte(int idCliente)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");


            SqlCommand comando = new SqlCommand("usp_SELECT_Mediciones_Reporte");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@idCliente", idCliente);

            DataSet ds = db.ExecuteReader(comando, "Medicion");
            return ds;
        }


        // Create
        public static void Modificar(Medicion medicion)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@id", medicion.id);



            db.ExecuteNonQuery(comando);
        }





        // Create
        public static void Eliminar(int id)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("usp_DELETE_Medicion_ByIDCliente");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@idCliente", id);

            db.ExecuteNonQuery(comando);
        }

    }//fin class
}
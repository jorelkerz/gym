﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebAppGym.Clases;
using WebAppGym.Persistencia;

namespace WebAppGym.DAL
{
    public class DAL_Cardio
    {
        // Create
        public static void Insertar(Cardio cardio)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("usp_INSERT_Cardio");
            comando.CommandType = CommandType.StoredProcedure;


            comando.Parameters.AddWithValue("@nombre", cardio.nombre);
            comando.Parameters.AddWithValue("@ejercicio", cardio.ejercicio);
            comando.Parameters.AddWithValue("@url", cardio.url);
            comando.Parameters.AddWithValue("@tiempo", cardio.tiempo);
            comando.Parameters.AddWithValue("@descanso", cardio.descanso);
            comando.Parameters.AddWithValue("@calorias", cardio.calorias);

            db.ExecuteNonQuery(comando);
        }

        // Read
        public static DataSet SeleccionarTodos()
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("usp_SELECT_Cardio_All");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;

            DataSet ds = db.ExecuteReader(comando, "Cardio");
            return ds;
        }

        // Create
        public static void Modificar(Cardio cardio)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("usp_UPDATE_Cardio");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@id", cardio.id);
            comando.Parameters.AddWithValue("@nombre", cardio.nombre);
            comando.Parameters.AddWithValue("@ejercicio", cardio.ejercicio);
            comando.Parameters.AddWithValue("@url", cardio.url);
            comando.Parameters.AddWithValue("@tiempo", cardio.tiempo);
            comando.Parameters.AddWithValue("@descanso", cardio.descanso);
            comando.Parameters.AddWithValue("@calorias", cardio.calorias);

            db.ExecuteNonQuery(comando);
        }

        // Create
        public static void Eliminar(int id)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("usp_DELETE_Cardio_ByID");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@id", id);

            db.ExecuteNonQuery(comando);
        }
    }//fin cardio
}
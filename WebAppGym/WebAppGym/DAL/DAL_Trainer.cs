﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebAppGym.Clases;
using WebAppGym.Persistencia;

namespace WebAppGym.DAL
{
    public class DAL_Trainer
    {
        public static void Insertar(RutinaCliente rutinaCliente)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("usp_INSERT_Trainer");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@idCliente", rutinaCliente.idCliente);
            comando.Parameters.AddWithValue("@dia", rutinaCliente.dia);
            comando.Parameters.AddWithValue("@idHipertrofia", rutinaCliente.idHipertrofia);
            comando.Parameters.AddWithValue("@idCardio", rutinaCliente.idCardio);

            db.ExecuteNonQuery(comando);
        }

        public static DataSet SeleccionarTodos()
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("usp_SELECT_Trainer_All");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;

            DataSet ds = db.ExecuteReader(comando, "RutinaCliente");
            return ds;
        }

        public static void Borrar(RutinaCliente rutinaCliente)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("usp_DELETE_Trainer");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@idCliente", rutinaCliente.idCliente);
            comando.Parameters.AddWithValue("@dia", rutinaCliente.dia);
            comando.Parameters.AddWithValue("@idHipertrofia", rutinaCliente.idHipertrofia);
            comando.Parameters.AddWithValue("@idCardio", rutinaCliente.idCardio);

            db.ExecuteNonQuery(comando);
        }
    }
}
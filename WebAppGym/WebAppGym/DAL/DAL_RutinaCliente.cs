﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebAppGym.Clases;
using WebAppGym.Persistencia;

namespace WebAppGym.DAL
{
    public class DAL_RutinaCliente
    {

        // Create
        public static void Insertar(RutinaCliente rutinaCliente)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@id", rutinaCliente.id);



            db.ExecuteNonQuery(comando);
        }

        // Read
        public static DataSet SeleccionarTodos()
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;

            DataSet ds = db.ExecuteReader(comando, "RutinaCliente");
            return ds;
        }



        // Read
        public static DataSet SeleccionarRutinaClientePorIDCliente(int idCliente)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");


            SqlCommand comando = new SqlCommand("usp_SELECT_RutinaClienteAll_ByID");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@idCliente", idCliente);

            DataSet ds = db.ExecuteReader(comando, "RutinaCliente");
            return ds;
        }


        // Create
        public static void Modificar(Medicion medicion)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@id", medicion.id);



            db.ExecuteNonQuery(comando);
        }





        // Create
        public static void Eliminar(int id)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@id", id);

            db.ExecuteNonQuery(comando);
        }

        public static DataSet ReporteGrafico()
        {
            Database db = DatabaseFactory.CreateDatabase("Default");


            SqlCommand comando = new SqlCommand("usp_Repote_Grafico");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;

            DataSet ds = db.ExecuteReader(comando, "RutinaCliente");
            return ds;
        }
    }
}
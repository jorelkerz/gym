﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebAppGym.Clases;
using WebAppGym.Persistencia;

namespace WebAppGym.DAL
{
    public class DAL_Hipertrofia
    {
        // Create
        public static void Insertar(Hipertrofia hipertrofia)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("usp_INSERT_Hipertrofia");
            comando.CommandType = CommandType.StoredProcedure;

          
            comando.Parameters.AddWithValue("@nombre", hipertrofia.nombre);
            comando.Parameters.AddWithValue("@ejercicio", hipertrofia.ejercicio);
            comando.Parameters.AddWithValue("@url", hipertrofia.url);
            comando.Parameters.AddWithValue("@series", hipertrofia.serie);
            comando.Parameters.AddWithValue("@repeticiones", hipertrofia.repeticion);
            comando.Parameters.AddWithValue("@descanso", hipertrofia.descanso);
            comando.Parameters.AddWithValue("@modo", hipertrofia.modo);
            comando.Parameters.AddWithValue("@calorias", hipertrofia.calorias);

            db.ExecuteNonQuery(comando);
        }

        // Read
        public static DataSet SeleccionarTodos()
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("usp_SELECT_Hipertrofia_All");
            // Es requerido indicar que el tipo es un StoreProcedure
            comando.CommandType = CommandType.StoredProcedure;

            DataSet ds = db.ExecuteReader(comando, "Hipertrofia");
            return ds;
        }

        // Create
        public static void Modificar(Hipertrofia hipertrofia)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("usp_UPDATE_Hipertrofia");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@id", hipertrofia.id);
            comando.Parameters.AddWithValue("@nombre", hipertrofia.nombre);
            comando.Parameters.AddWithValue("@ejercicio", hipertrofia.ejercicio);
            comando.Parameters.AddWithValue("@url", hipertrofia.url);
            comando.Parameters.AddWithValue("@series", hipertrofia.serie);
            comando.Parameters.AddWithValue("@repeticiones", hipertrofia.repeticion);
            comando.Parameters.AddWithValue("@descanso", hipertrofia.descanso);
            comando.Parameters.AddWithValue("@modo", hipertrofia.modo);
            comando.Parameters.AddWithValue("@calorias", hipertrofia.calorias);

            db.ExecuteNonQuery(comando);
        }

        // Create
        public static void Eliminar(int id)
        {
            Database db = DatabaseFactory.CreateDatabase("Default");

            SqlCommand comando = new SqlCommand("usp_DELETE_Hipertrofia_ByID");
            comando.CommandType = CommandType.StoredProcedure;

            comando.Parameters.AddWithValue("@id", id);

            db.ExecuteNonQuery(comando);
        }
    }
}
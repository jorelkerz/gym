﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Perfil/perfilSocioMaster.Master" AutoEventWireup="true" CodeBehind="socios.aspx.cs" Inherits="WebAppGym.Vistas.Perfil.socios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <div class="contenedorDatosSocios ">
        <div class="datosPersonales">
            <section id="sectnombre">
                <asp:Label ID="lblNombre" runat="server" Text="nombre"></asp:Label>
            </section>
            <section id="sectcorreo">
                <asp:Label ID="lblCorreo" runat="server" Text="correo"></asp:Label><br />
                <asp:Label ID="lblAltura" runat="server" Text="altura"></asp:Label>
            </section>
            <section id="secttelefono">
                <asp:Label ID="lblTelefono" runat="server" Text="telefono"></asp:Label>
            </section>
            <section id="sectnacimiento">
                <asp:Label ID="lblfechaNacimiento" runat="server" Text="nacimiento"></asp:Label>
            </section>
        </div>
        <div class="medidas">
            <section id="sectpeso">
                <asp:Label ID="lblPeso" runat="server" Text="peso"></asp:Label>
            </section>
            <section id="sectritmocardiaco">
                <asp:Label ID="lblRitmoCardiaco" runat="server" Text="ritmocardiaco"></asp:Label>
            </section>
            <section id="sectbrazorelajado">
                <asp:Label ID="lblBrazoRelajado" runat="server" Text="brazorelajado"></asp:Label>
            </section>
            <section id="sectbrazotenso">
                <asp:Label ID="lblBrazoTenso" runat="server" Text="brazotenso"></asp:Label>
            </section>
            <section id="sectantebrazo">
                <asp:Label ID="lblAntebrazo" runat="server" Text="antebrazo"></asp:Label>
            </section>
            <section id="sectmuslo">
                <asp:Label ID="lblMuslo" runat="server" Text="muslo"></asp:Label>
            </section>
            <section id="sectpechonormal">
                <asp:Label ID="lblPechoNormal" runat="server" Text="pechonormal"></asp:Label>
            </section>
            <section id="sectpechohinchado">
                <asp:Label ID="lblPechoHinchado" runat="server" Text="pechohinchado"></asp:Label>
            </section>
            <section id="sectgemelos">
                <asp:Label ID="lblGemelos" runat="server" Text="gemelos"></asp:Label>
            </section>
            <section id="sectcintura">
                <asp:Label ID="lblCintura" runat="server" Text="cintura"></asp:Label>
            </section>
            <section id="sectcadera">
                <asp:Label ID="lblCadera" runat="server" Text="cadera"></asp:Label>
            </section>
            <section id="sectporcetajegrasa">
                <asp:Label ID="lblPorcentajeGrasa" runat="server" Text="porcetajegrasa"></asp:Label>
            </section>
            <section id="sectIMC">
                <asp:Label ID="lblIMC" runat="server" Text="imc"></asp:Label>
            </section>
            <section id="sectUltimaLectura">
                <asp:Label ID="lblfechaRegistro" runat="server" Text="UltimaLectura"></asp:Label>
            </section>
        </div>

        <section id="titulo">
            <asp:Label ID="Label1" runat="server" Text="Dieta"></asp:Label>
        </section>
        <section id="descripcionDieta">
            <asp:Label ID="lbldescripcionDieta" runat="server" Text="Descripcion"></asp:Label>
        </section>
        <%--<section id="calorias">
            <asp:Label ID="lblcaloriasDieta" runat="server" Text="Calorias"></asp:Label>
        </section>
        <section id="proteinas">
            <asp:Label ID="lblproteinasDieta" runat="server" Text="Proteinas"></asp:Label>
        </section>--%>

    </div>

    <%--<div class="contenedorDieta">
        
    </div>--%>




</asp:Content>

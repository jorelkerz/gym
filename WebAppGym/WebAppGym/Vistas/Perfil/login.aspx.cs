﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebAppGym.Clases;
using WebAppGym.Singleton;

namespace WebAppGym.Vistas.Perfil
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            try
            {
                Cliente cliente = GestorTrainer.ObtenerClienteporID(Convert.ToInt32(cedula.Text));
                
                int id = Convert.ToInt32(cedula.Text);
                String pass = clave.Text;

                if (id == cliente.id)
                {
                    if (pass.Equals(cliente.contrasena))
                    {
                        if (Session["cedula"] != null)
                        {
                            Session.Remove("cedula");
                        }

                        Session.Add("cedula", id);
                        GestorTrainer.idSeccion = (int) (Session["cedula"]);
                        Response.Redirect("socios.aspx");
                    }
                    else
                    {
                        Response.Write("<script>alert('Clave invalida');</script>");
                        clave.Text = "";
                    }
                }
                else
                {
                    Response.Write("<script>alert('Usuario invalido');</script>");
                    cedula.Text = "";
                }


            }
            catch
            {
                Response.Write("<script>alert('Su cedula no existe o esta mal digitada');</script>");
                clave.Text = "";
            }




        }
    }
}
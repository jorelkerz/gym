﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Perfil/perfilTrainerMaster.Master" AutoEventWireup="true" CodeBehind="mantenimientoCliente.aspx.cs" Inherits="WebAppGym.Vistas.Perfil.mantenimientoCliente" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .auto-style1 {
            text-align: center;
        }
        .auto-style2 {
            color: #FF0000;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="contenedorDatosSocios ">
                <div class="datosPersonales">
                    <p>Datos Personales</p>
                    <table align="center">
                        <tr>
                            <td><asp:Label ID="lblCedula" runat="server" Text="Cedula: "></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtCedula" runat="server" placeholder="Cedula"></asp:TextBox>
                                <asp:MaskedEditExtender ID="txtIdentificacion_MaskedEditExtender" runat="server" Century="2000" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" Mask="999999999" TargetControlID="txtCedula"></asp:MaskedEditExtender>
                            </td>
                            <td></td>
                            <td><asp:Label ID="lblGenero" runat="server" Text="Genero: "></asp:Label></td>
                            <td>
                                <asp:DropDownList ID="ddlGenero" runat="server">
                                    <asp:ListItem>Masculino</asp:ListItem>
                                    <asp:ListItem>Femenino</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="lblNombre" runat="server" Text="Nombre: "></asp:Label></td>
                            <td><asp:TextBox ID="txtNombre" runat="server" placeholder="Nombre"></asp:TextBox></td>
                            <td></td>
                            <td><asp:Label ID="lblCorreo" runat="server" Text="Correo: "></asp:Label></td>
                            <td><asp:TextBox ID="txtCorreo" runat="server" placeholder="Correo"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="lblTelefono" runat="server" Text="Telefono: " style="text-align: left"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtTelefono" runat="server" placeholder="Telefono"></asp:TextBox>
                                <asp:MaskedEditExtender ID="txtTelefono_MaskedEditExtender" runat="server" Century="2000" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder="" CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder="" CultureTimePlaceholder="" Enabled="True" Mask="99999999" TargetControlID="txtTelefono"></asp:MaskedEditExtender>
                            </td>
                            <td></td>
                            <td><asp:Label ID="lblfechaNacimiento" runat="server" Text="Fecha Nacimiento:" ></asp:Label></td>
                            <td>
                                <asp:TextBox runat="server" ID="txtFechaN" placeholder="Fecha Nacimiento"/>
                                <asp:ImageButton runat="Server" ID="Image1" ImageUrl="~/Vistas/Perfil/images/Calendar_scheduleHS.png" AlternateText="Click to show calendar" /><br />
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" TargetControlID="txtFechaN" PopupButtonID="Image1"></asp:CalendarExtender>
                            </td>
                        </tr>
                    </table>
                </div>
                <p>Medidas</p>
                <div class="medidas">
                    <table align="center">
                        <tr>
                            <td><asp:Label ID="lblPeso" runat="server" Text="Peso:" style="text-align: left"></asp:Label></td>
                            <td><asp:TextBox ID="txtPeso" runat="server" placeholder="Peso" TextMode="Number"></asp:TextBox></td>
                            <td></td>
                            <td><asp:Label ID="lblRitmoCardiaco" runat="server" Text="Ritmo Cardiaco: "></asp:Label></td>
                            <td><asp:TextBox ID="txtRitmoC" runat="server" placeholder="Ritmo Cardiaco" TextMode="Number"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="lblBrazoRelajado" runat="server" Text="Brazo Relajado: "></asp:Label></td>
                            <td><asp:TextBox ID="txtBrazoR" runat="server" placeholder="Brazo Relajado" TextMode="Number"></asp:TextBox></td>
                            <td></td>                        
                            <td><asp:Label ID="lblBrazoTenso" runat="server" Text="Brazo Tenso: "></asp:Label></td>
                            <td><asp:TextBox ID="txtBrazoT" runat="server" placeholder="Brazo Tenso" TextMode="Number"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="lblAntebrazo" runat="server" Text="Antebrazo:"></asp:Label></td>
                            <td><asp:TextBox ID="txtAntebrazo" runat="server" placeholder="Antebrazo" TextMode="Number"></asp:TextBox></td>
                            <td></td>                        
                            <td><asp:Label ID="lblMuslo" runat="server" Text="Muslo:"></asp:Label></td>
                            <td><asp:TextBox ID="txtMuslo" runat="server" placeholder="Muslo" TextMode="Number"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="lblPechoNormal" runat="server" Text="Pecho Normal: "></asp:Label></td>
                            <td><asp:TextBox ID="txtPechoN" runat="server" placeholder="Pecho Normal" TextMode="Number"></asp:TextBox></td>
                            <td></td>                        
                            <td><asp:Label ID="lblPechoHinchado" runat="server" Text="Pechoh Inchado: "></asp:Label></td>
                            <td><asp:TextBox ID="txtPechoI" runat="server" placeholder="Pecho Inchado" TextMode="Number"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="lblGemelos" runat="server" Text="Gemelos: "></asp:Label></td>
                            <td><asp:TextBox ID="txtGemelos" runat="server" placeholder="Gemelos" TextMode="Number"></asp:TextBox></td>
                            <td></td>                        
                            <td><asp:Label ID="lblCintura" runat="server" Text="Cintura: "></asp:Label></td>
                            <td><asp:TextBox ID="txtCintura" runat="server" placeholder="Cintura" TextMode="Number"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="lblCadera" runat="server" Text="Cadera: "></asp:Label></td>
                            <td><asp:TextBox ID="txtCadera" runat="server" placeholder="Cadera" TextMode="Number"></asp:TextBox></td>
                            <td></td>                        
                            <td><asp:Label ID="lblPorcentajeGrasa" runat="server" Text="Porcetaje Grasa: "></asp:Label></td>
                            <td><asp:TextBox ID="txtPorcentajeG" runat="server" placeholder="Procentaje Grasa" TextMode="Number"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td><asp:Label ID="lblAltura" runat="server" Text="Altura (cm): "></asp:Label></td>
                            <td><asp:TextBox ID="txtAltura" runat="server" placeholder="Altura (cm)" TextMode="Number"></asp:TextBox></td>
                            <td></td>
                            <td><asp:Label ID="lblIMC" runat="server" Text="IMC: " Visible="false"></asp:Label></td>
                            <td><asp:Label ID="lblIMC2" runat="server" Text=" " Visible="false"></asp:Label></td>
                        </tr>
                        <tr>
                            <td colspan="5" class="auto-style1"><strong><asp:Label ID="lblMensaje" runat="server" Text=" " CssClass="auto-style2"></asp:Label></strong></td>
                        </tr>
                    </table>
                </div>

                <section id="titulo">
                    <asp:Label ID="Label1" runat="server" Text="Descripcion Dieta"></asp:Label><br />
                    <asp:TextBox ID="txtDietaD" runat="server" TextMode="MultiLine" placeholder="Descripcion Dieta" Height="81px" Width="266px"></asp:TextBox>
                </section>

        
                <table align="center">
                    <tr>
                        <td><asp:Button ID="btnAceptar" runat="server" Text="Aceptar" OnClick="btnAceptar_Click"/></td>
                        <td><asp:Button ID="btnCancelar" runat="server" Text="Cancelar" OnClick="btnCancelar_Click"/></td>
                    </tr>
                </table>
        
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    

    <div class="tablaMantenimientos">
        <asp:GridView ID="gvCliente" runat="server" AutoGenerateDeleteButton="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="id" ForeColor="#333333" GridLines="None" Style="text-align: center" OnRowCommand="gvCliente_RowCommand" OnRowDeleting="gvCliente_RowDeleting">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:ButtonField Text="Actualizar" CommandName="Actualizar" />
                <asp:BoundField DataField="id" HeaderText="ID" />
                <asp:BoundField DataField="nombre" HeaderText="Nombre" />
                <asp:BoundField DataField="fechaNacimiento" HeaderText="Fecha Nacimiento" />
                <asp:BoundField DataField="fechaRegistro" HeaderText="Fecha Registro" ReadOnly="True" />
                <asp:BoundField DataField="genero" HeaderText="Genero" />
                <asp:BoundField DataField="telefono" HeaderText="Telefono" />
                <asp:BoundField DataField="email" HeaderText="Correo" />
                <asp:BoundField DataField="descripcionDieta" HeaderText="Dieta" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
    </div>


</asp:Content>

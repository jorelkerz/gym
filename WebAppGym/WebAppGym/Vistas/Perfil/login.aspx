﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="WebAppGym.Vistas.Perfil.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
   
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Login</title>
    <link href="../../css/animate2.css" rel="stylesheet" />
    <link href="../../css/style2.css" rel="stylesheet" />
    <link href="../../css/cssgoogle1.css" rel="stylesheet" />
    <script src="../../js/jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div class="container">
            <div class="top">
                <h1 id="title" class="hidden"><span id="logo">MY GYM</span></h1>
            </div>
            <div class="login-box animated fadeInDown">
                <div class="box-header">
                    <h2>Log In</h2>
                </div>
                <br />
                <asp:TextBox ID="cedula" runat="server" placeholder="Cedula" ></asp:TextBox>             
                <br />
                <br />
                <asp:TextBox ID="clave" runat="server" TextMode="Password" placeholder="Clave"></asp:TextBox>
                <br />
                <asp:Button ID="Button1" runat="server" Text="Aceptar" OnClick="Button1_Click" />
                <br />
            </div>
        </div>
    </div>
    </form>
</body>

    <script>
        $(document).ready(function () {
            $('#logo').addClass('animated fadeInDown');
            $("input:text:visible:first").focus();
        });
        $('#username').focus(function () {
            $('label[for="username"]').addClass('selected');
        });
        $('#username').blur(function () {
            $('label[for="username"]').removeClass('selected');
        });
        $('#password').focus(function () {
            $('label[for="password"]').addClass('selected');
        });
        $('#password').blur(function () {
            $('label[for="password"]').removeClass('selected');
        });
</script>
</html>

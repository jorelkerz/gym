﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebAppGym.Singleton;
using WebAppGym.Clases;

namespace WebAppGym.Vistas.Perfil
{
    public partial class mantenimientoCardio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                urlRutinas.DataSource = GestorTrainer.GetInstacia().cargaURLCardio();
                urlRutinas.DataTextField = "nombre";
                urlRutinas.DataValueField = "direccion";
                urlRutinas.DataBind();


                dropTiempo.DataSource = GestorTrainer.GetInstacia().cargaTiempo();
                dropTiempo.DataBind();

                dropDescanso.DataSource = GestorTrainer.GetInstacia().cargarDescansos();
                dropDescanso.DataBind();


                Refrescar();
            }
        }

        private void Refrescar()
        {

            gvCardio.DataSource = GestorTrainer.ListaCardio();

            gvCardio.DataBind();
        }

        protected void gvCardio_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvCardio.EditIndex = -1;

            Refrescar();
        }

        protected void gvCardio_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvCardio.EditIndex = e.NewEditIndex;

            Refrescar();

            List<urlRutinas> urlRutinas = GestorTrainer.GetInstacia().cargaURLCardio();
            //urlRutinas.RemoveAt(0);
            ((DropDownList)gvCardio.Rows[e.NewEditIndex].FindControl("ddlTipoEjerGriew")).DataSource = urlRutinas;
            ((DropDownList)gvCardio.Rows[e.NewEditIndex].FindControl("ddlTipoEjerGriew")).DataTextField = "nombre";
            ((DropDownList)gvCardio.Rows[e.NewEditIndex].FindControl("ddlTipoEjerGriew")).DataValueField = "direccion";
            ((DropDownList)gvCardio.Rows[e.NewEditIndex].FindControl("ddlTipoEjerGriew")).DataBind();

            List<string> tiempo = GestorTrainer.GetInstacia().cargaTiempo();
            tiempo.RemoveAt(0);
            ((DropDownList)gvCardio.Rows[e.NewEditIndex].FindControl("ddlTiempoGriew")).DataSource = tiempo;
            ((DropDownList)gvCardio.Rows[e.NewEditIndex].FindControl("ddlTiempoGriew")).DataBind();

            List<string> des = GestorTrainer.GetInstacia().cargarDescansos();
            des.RemoveAt(0);
            ((DropDownList)gvCardio.Rows[e.NewEditIndex].FindControl("ddlDescansoGriew")).DataSource = des;
            ((DropDownList)gvCardio.Rows[e.NewEditIndex].FindControl("ddlDescansoGriew")).DataBind();


        }

        protected void gvCardio_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            String codigo = gvCardio.DataKeys[e.RowIndex].Value.ToString();
            int id = Convert.ToInt32(codigo);
            GestorTrainer.EliminarCardio(id);
            gvCardio.EditIndex = -1;
            Refrescar();
        }

        protected void gvCardio_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

            // Se lanza cuando se hace clic en Actualizar
            Response.Write(e.RowIndex.ToString());

            // Se obtiene el TextBox que esta en la celda codigo de la fila por editar
            //String codigo = ((TextBox)gvCardio.Rows[e.RowIndex].Cells[1].Controls[0]).Text;
            String codigo = gvCardio.DataKeys[e.RowIndex].Value.ToString();
            int id = 0;
            id = Convert.ToInt32(codigo);

            String nombre = ((TextBox)gvCardio.Rows[e.RowIndex].Cells[1].Controls[0]).Text;
            String ejercicio = ((DropDownList)gvCardio.Rows[e.RowIndex].FindControl("ddlTipoEjerGriew")).SelectedItem.Text;
            String url = ((DropDownList)gvCardio.Rows[e.RowIndex].FindControl("ddlTipoEjerGriew")).SelectedValue;
            String tiempo = ((DropDownList)gvCardio.Rows[e.RowIndex].FindControl("ddlTiempoGriew")).SelectedValue;
            String descanso = ((DropDownList)gvCardio.Rows[e.RowIndex].FindControl("ddlDescansoGriew")).SelectedValue;
            String calorias = GestorTrainer.calculoCaloriasCardio(tiempo);

            GestorTrainer.ModificarCardio(new Clases.Cardio(id, nombre, ejercicio, url, tiempo, descanso, calorias));

            gvCardio.EditIndex = -1;
            Refrescar();
        }



        protected void Button1_Click(object sender, EventArgs e)
        {

            Cardio c = new Cardio();
            c.nombre = Nombre.Text;
            c.ejercicio = urlRutinas.SelectedItem.Text;
            c.url = urlRutinas.SelectedValue;
            c.tiempo = dropTiempo.SelectedValue;
            c.descanso = dropDescanso.SelectedValue;
            c.calorias = GestorTrainer.calculoCaloriasCardio(c.tiempo);

            GestorTrainer.InsertarCardio(c);
            Refrescar();

        }





    }//fin class
}
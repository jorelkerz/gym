﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebAppGym.Singleton;

namespace WebAppGym.Vistas.Perfil
{
    public partial class passwordSocio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (textPass1.Text.Equals(textPass2.Text))
            {
                int id = GestorTrainer.idSeccion;
                String contrasena = textPass1.Text;
                GestorTrainer.ModificarClienteContrasena(id, contrasena);
                Response.Write("<script>alert('Las claves se ha cambiado!!!');</script>");

            }
            else
            {
                Response.Write("<script>alert('Las claves no son iguales!!!');</script>");
            }
        }
    }
}
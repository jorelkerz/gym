﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="loginTrainers.aspx.cs" Inherits="WebAppGym.Vistas.Perfil.loginTrainers" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Login</title>
    <link href="../../css/animate2.css" rel="stylesheet" />
    <link href="../../css/style2.css" rel="stylesheet" />
    <link href="../../css/cssgoogle1.css" rel="stylesheet" />
    <link href="../../css/jlcss.css" rel="stylesheet" />
    <script src="../../js/jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div class="container">
            <div class="top">
                <h1 id="title" class="hidden"><span id="logo">MY GYM</span></h1>
            </div>
            <div class="login-box animated fadeInDown">
                <%--<div class="box-header">
                    <h2>Log In</h2>
                </div>--%>
                <asp:LoginView ID="LoginView1" runat="server">
                    <AnonymousTemplate>
                        <asp:Login ID="Login1" runat="server" CreateUserText="Registrar Administrador" CreateUserUrl="~/Vistas/Perfil/registroAdmin.aspx"></asp:Login>
                    </AnonymousTemplate>
                    <LoggedInTemplate>
                        <%--<div class="contenedorDatosSocios ">--%>
                            <div class="datosPersonales">
                                <p> Bienvenido: 
                                <asp:LoginName ID="LoginName1" runat="server" /><br />
                                
                                <span>Elija una opcion para seguir:
                                    <ul>
                                        <li><asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Vistas/Perfil/mantenimientoCliente.aspx">Administracion Gym</asp:HyperLink></li>
                                        <li><asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Vistas/Perfil/cambioClaveAdmin.aspx">Actualizar Clave</asp:HyperLink></li>
                                    </ul>
                                </span>

                                <asp:LoginStatus ID="LoginStatus1" runat="server" />
                                </p>
                            </div>
                        <%--</div>--%>
                    </LoggedInTemplate>
                </asp:LoginView>
            </div>
        </div>
    </div>
    </form>
</body>

<script>
    $(document).ready(function () {
        $('#logo').addClass('animated fadeInDown');
        $("input:text:visible:first").focus();
    });
</script>
</html>

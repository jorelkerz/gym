﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebAppGym.Singleton;

namespace WebAppGym.Vistas.Perfil
{
    public partial class TrainerpasswordSocio : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(Textcedula.Text);
                String contrasena = Textcedula.Text;
                GestorTrainer.ModificarClienteContrasena(id, contrasena);
                Response.Write("<script>alert('Las clave se ha reseteado!!!');</script>");

            }
            catch
            {
                Response.Write("<script>alert('Las cedula no existe!!!');</script>");
            }

            Textcedula.Text = "";
        }
    }
}
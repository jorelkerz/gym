﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="registroAdmin.aspx.cs" Inherits="WebAppGym.Vistas.Perfil.registroAdmin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Crear Administrador</title>
    <link href="../../css/animate2.css" rel="stylesheet" />
    <link href="../../css/style2.css" rel="stylesheet" />
    <link href="../../css/cssgoogle1.css" rel="stylesheet" />
    <link href="../../css/jlcss.css" rel="stylesheet" />
    <script src="../../js/jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="container">
                <div class="top">
                    <h1 id="title" class="hidden"><span id="logo">MY GYM</span></h1>
                </div>
                <div class="login-box animated fadeInDown">
                    <div class="datosPersonales">
                        <asp:CreateUserWizard ID="CreateUserWizard1" runat="server" OnContinueButtonClick="CreateUserWizard1_ContinueButtonClick">
                            <WizardSteps>
                                <asp:CreateUserWizardStep ID="CreateUserWizardStep1" runat="server">
                                </asp:CreateUserWizardStep>
                                <asp:CompleteWizardStep ID="CompleteWizardStep1" runat="server">
                                </asp:CompleteWizardStep>
                            </WizardSteps>
                            <FinishNavigationTemplate>
                                <asp:Button ID="FinishPreviousButton" runat="server" CausesValidation="False" CommandName="MovePrevious" Text="Previous" />
                                <asp:Button ID="FinishButton" runat="server" CommandName="MoveComplete" Text="Finish" />
                            </FinishNavigationTemplate>
                            <StartNavigationTemplate>
                                <asp:Button ID="StartNextButton" runat="server" CommandName="MoveNext" Text="Next" />
                            </StartNavigationTemplate>
                        </asp:CreateUserWizard>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>

<script>
    $(document).ready(function () {
        $('#logo').addClass('animated fadeInDown');
        $("input:text:visible:first").focus();
    });
</script>
</html>

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebAppGym.Clases;
using WebAppGym.Singleton;

namespace WebAppGym.Vistas.Perfil
{
    public partial class mantenimientoHipertrofia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                urlRutinas.DataSource = GestorTrainer.GetInstacia().urlRutinas();
                urlRutinas.DataTextField = "nombre";
                urlRutinas.DataValueField = "direccion";
                urlRutinas.DataBind();


                dropSeries.DataSource = GestorTrainer.GetInstacia().cargarSeries();
                dropSeries.DataBind();

                dropRepeticion.DataSource = GestorTrainer.GetInstacia().cargarRepeticiones();
                dropRepeticion.DataBind();

                dropDescanso.DataSource = GestorTrainer.GetInstacia().cargarDescansos();
                dropDescanso.DataBind();

                dropModo.DataSource = GestorTrainer.GetInstacia().cargarModo();
                dropModo.DataBind();
                Refrescar();

            }

        }

        private void Refrescar()
        {

            gvHipertrofia.DataSource = GestorTrainer.ListaHipertrofia();

            gvHipertrofia.DataBind();
        }

        protected void gvHipertrofia_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gvHipertrofia.EditIndex = -1;

            Refrescar();
        }

        protected void gvHipertrofia_RowEditing(object sender, GridViewEditEventArgs e)
        {
            gvHipertrofia.EditIndex = e.NewEditIndex;

            Refrescar();

            List<urlRutinas> urlRutinas = GestorTrainer.GetInstacia().urlRutinas();
           // urlRutinas.RemoveAt(0);
            ((DropDownList)gvHipertrofia.Rows[e.NewEditIndex].FindControl("ddlTipoEjerGriew")).DataSource = urlRutinas;
            ((DropDownList)gvHipertrofia.Rows[e.NewEditIndex].FindControl("ddlTipoEjerGriew")).DataTextField = "nombre";
            ((DropDownList)gvHipertrofia.Rows[e.NewEditIndex].FindControl("ddlTipoEjerGriew")).DataValueField = "direccion";
            ((DropDownList)gvHipertrofia.Rows[e.NewEditIndex].FindControl("ddlTipoEjerGriew")).DataBind();

            List<string> series = GestorTrainer.GetInstacia().cargarSeries();
            series.RemoveAt(0);
            ((DropDownList)gvHipertrofia.Rows[e.NewEditIndex].FindControl("ddlSeriesGriew")).DataSource = series;
            ((DropDownList)gvHipertrofia.Rows[e.NewEditIndex].FindControl("ddlSeriesGriew")).DataBind();


            List<string> rep = GestorTrainer.GetInstacia().cargarRepeticiones();
            rep.RemoveAt(0);
            ((DropDownList)gvHipertrofia.Rows[e.NewEditIndex].FindControl("ddlRepeticionesGriew")).DataSource = rep;
            ((DropDownList)gvHipertrofia.Rows[e.NewEditIndex].FindControl("ddlRepeticionesGriew")).DataBind();

            List<string> des = GestorTrainer.GetInstacia().cargarDescansos();
            des.RemoveAt(0);
            ((DropDownList)gvHipertrofia.Rows[e.NewEditIndex].FindControl("ddlDescansoGriew")).DataSource = des;
            ((DropDownList)gvHipertrofia.Rows[e.NewEditIndex].FindControl("ddlDescansoGriew")).DataBind();

            List<string> modo = GestorTrainer.GetInstacia().cargarModo();
            modo.RemoveAt(0);
            ((DropDownList)gvHipertrofia.Rows[e.NewEditIndex].FindControl("ddlModoGriew")).DataSource = modo;
            ((DropDownList)gvHipertrofia.Rows[e.NewEditIndex].FindControl("ddlModoGriew")).DataBind();
        }

        protected void gvHipertrofia_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {


            String codigo = gvHipertrofia.DataKeys[e.RowIndex].Value.ToString();
            int id = Convert.ToInt32(codigo);
            GestorTrainer.Eliminar(id);
            gvHipertrofia.EditIndex = -1;
            Refrescar();
        }

        protected void gvHipertrofia_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {

            // Se lanza cuando se hace clic en Actualizar
            Response.Write(e.RowIndex.ToString());

            // Se obtiene el TextBox que esta en la celda codigo de la fila por editar
            //String codigo = ((TextBox)gvHipertrofia.Rows[e.RowIndex].Cells[1].Controls[0]).Text;
            String codigo = gvHipertrofia.DataKeys[e.RowIndex].Value.ToString();
            int id = 0;
            id = Convert.ToInt32(codigo);
   
            String nombre = ((TextBox)gvHipertrofia.Rows[e.RowIndex].Cells[1].Controls[0]).Text;
            String ejercicio = ((DropDownList)gvHipertrofia.Rows[e.RowIndex].FindControl("ddlTipoEjerGriew")).SelectedItem.Text;
            String url = ((DropDownList)gvHipertrofia.Rows[e.RowIndex].FindControl("ddlTipoEjerGriew")).SelectedValue;
            String serie = ((DropDownList)gvHipertrofia.Rows[e.RowIndex].FindControl("ddlSeriesGriew")).SelectedValue;
            String repeticion = ((DropDownList)gvHipertrofia.Rows[e.RowIndex].FindControl("ddlRepeticionesGriew")).SelectedValue;
            String descanso = ((DropDownList)gvHipertrofia.Rows[e.RowIndex].FindControl("ddlDescansoGriew")).SelectedValue;
            String modo = ((DropDownList)gvHipertrofia.Rows[e.RowIndex].FindControl("ddlModoGriew")).SelectedValue;
            String calorias = GestorTrainer.calculoCalorias(serie,repeticion);

            GestorTrainer.Modificar(new Clases.Hipertrofia(id, nombre,ejercicio,url, descanso, calorias, serie, repeticion, modo));

            gvHipertrofia.EditIndex = -1;
            Refrescar();
        }



        protected void Button1_Click(object sender, EventArgs e)
        {

            Hipertrofia h = new Hipertrofia();
            h.nombre = Nombre.Text;
            h.ejercicio = urlRutinas.SelectedItem.Text;
            h.url = urlRutinas.SelectedValue;
            h.serie = dropSeries.SelectedValue;
            h.repeticion = dropRepeticion.SelectedValue;
            h.descanso = dropDescanso.SelectedValue;
            h.modo = dropModo.SelectedValue;
            h.calorias = GestorTrainer.calculoCalorias(h.serie, h.repeticion);

            GestorTrainer.Insertar(h);
            Refrescar();

        }









    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebAppGym.Clases;
using WebAppGym.Singleton;

namespace WebAppGym.Vistas.Perfil
{
    public partial class mantenimientoCliente : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Refrescar();
                txtCedula.ReadOnly = false;
            }
        }

        private void Refrescar()
        {

            gvCliente.DataSource = GestorTrainer.ListaClientes();

            gvCliente.DataBind();
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            txtCedula.ReadOnly = false;
            Limpiar();   
        }

        protected void gvCliente_RowCommand(Object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Actualizar")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = gvCliente.Rows[index];
                //int id = Convert.ToInt32(row.DataItem("id"));
                string idC = gvCliente.DataKeys[index].Value.ToString();
                int id = Convert.ToInt32(idC);
                Cliente oCliente = GestorTrainer.ObtenerClienteporID(id) as Cliente;

                Session.Add("ID", id);

                btnAceptar.Text = "Actualizar";
                lblIMC.Visible = true;
                lblIMC2.Visible = true;

                txtCedula.Text = oCliente.id.ToString();
                txtCedula.ReadOnly = true;
                txtNombre.Text = oCliente.nombre.ToString();
                txtCorreo.Text = oCliente.email.ToString();
                txtFechaN.Text = oCliente.fechaNacimiento.ToString().Substring(0, 10);
                txtTelefono.Text = oCliente.telefono.ToString();
                ddlGenero.SelectedValue = oCliente.genero.ToString();
                txtDietaD.Text = oCliente.descripcionDieta.ToString();

                Medicion oMedicion = GestorTrainer.ObtenerMedicionporIDCliente(id);
                
                txtPeso.Text = oMedicion.peso.ToString();
                txtRitmoC.Text = oMedicion.ritmoCardiaco.ToString();
                txtBrazoR.Text = oMedicion.brazoRelajado.ToString();
                txtBrazoT.Text = oMedicion.brazoTension.ToString();
                txtAntebrazo.Text = oMedicion.antebrazo.ToString();
                txtMuslo.Text = oMedicion.muslo.ToString();
                txtPechoN.Text = oMedicion.pechoNomal.ToString();
                txtPechoI.Text = oMedicion.pechoHinchado.ToString();
                txtGemelos.Text = oMedicion.gemelos.ToString();
                txtCintura.Text = oMedicion.cintura.ToString();
                txtCadera.Text = oMedicion.cadera.ToString();
                txtPorcentajeG.Text = oMedicion.porcentajeGrasa.ToString();
                txtAltura.Text = oMedicion.altura.ToString();
                lblIMC2.Text = oMedicion.IMC.ToString();
            }
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            lblMensaje.Text = "";
            try
            {
                if (int.Parse(txtCedula.Text) > 0 && txtNombre.Text != "" && txtCorreo.Text != "" && txtFechaN.Text != "" && int.Parse(txtTelefono.Text) > 0 && txtDietaD.Text != "" &&
                    Convert.ToDouble(txtPeso.Text) > 0 && Convert.ToDouble(txtRitmoC.Text) > 0 && Convert.ToDouble(txtBrazoR.Text) > 0 && Convert.ToDouble(txtBrazoT.Text) > 0 && Convert.ToDouble(txtAntebrazo.Text) > 0 &&
                    Convert.ToDouble(txtMuslo.Text) > 0 && Convert.ToDouble(txtPechoN.Text) > 0 && Convert.ToDouble(txtPechoI.Text) > 0 && Convert.ToDouble(txtGemelos.Text) > 0 && Convert.ToDouble(txtCintura.Text) > 0 &&
                    Convert.ToDouble(txtCadera.Text) > 0 && Convert.ToDouble(txtPorcentajeG.Text) > 0 && Convert.ToInt32(txtAltura.Text) > 0)
                {
                    //   
                    if (txtCedula.ReadOnly == true)
                    {
                        //Actualizar
                        Cliente oCliente = new Cliente();
                        oCliente.id = Convert.ToInt32(txtCedula.Text);
                        oCliente.nombre = txtNombre.Text;
                        oCliente.fechaNacimiento = Convert.ToDateTime(txtFechaN.Text);
                        oCliente.email = txtCorreo.Text;
                        oCliente.telefono = Convert.ToInt32(txtTelefono.Text);
                        oCliente.genero = ddlGenero.SelectedValue;
                        oCliente.descripcionDieta = txtDietaD.Text;

                        GestorTrainer.ModificarCliente(oCliente);
                    }
                    else
                    {
                        //Ingresar
                        Cliente oCliente = new Cliente();
                        oCliente.id = Convert.ToInt32(txtCedula.Text);
                        oCliente.contrasena = txtCedula.Text;
                        oCliente.nombre = txtNombre.Text;
                        oCliente.fechaNacimiento = Convert.ToDateTime(txtFechaN.Text.ToString());
                        oCliente.email = txtCorreo.Text;
                        oCliente.telefono = Convert.ToInt32(txtTelefono.Text);
                        oCliente.genero = ddlGenero.SelectedValue;
                        oCliente.descripcionDieta = txtDietaD.Text;

                        GestorTrainer.InsertarCliente(oCliente);
                    }

                    Medicion oMedicion = new Medicion();
                    oMedicion.id = Convert.ToInt32(txtCedula.Text);
                    oMedicion.peso = Convert.ToDouble(txtPeso.Text);
                    oMedicion.ritmoCardiaco = Convert.ToDouble(txtRitmoC.Text);
                    oMedicion.brazoRelajado = Convert.ToDouble(txtBrazoR.Text);
                    oMedicion.brazoTension = Convert.ToDouble(txtBrazoT.Text);
                    oMedicion.antebrazo = Convert.ToDouble(txtAntebrazo.Text);
                    oMedicion.muslo = Convert.ToDouble(txtMuslo.Text);
                    oMedicion.pechoNomal = Convert.ToDouble(txtPechoN.Text);
                    oMedicion.pechoHinchado = Convert.ToDouble(txtPechoI.Text);
                    oMedicion.gemelos = Convert.ToDouble(txtGemelos.Text);
                    oMedicion.cintura = Convert.ToDouble(txtCintura.Text);
                    oMedicion.cadera = Convert.ToDouble(txtCadera.Text);
                    oMedicion.porcentajeGrasa = Convert.ToDouble(txtPorcentajeG.Text);
                    oMedicion.altura = Convert.ToInt32(txtAltura.Text);

                    double denominador = Convert.ToDouble(oMedicion.altura) / 100d;
                    double IMC = oMedicion.peso / (denominador * denominador);

                    oMedicion.IMC = Math.Round(IMC,2);

                    GestorTrainer.InsertarMedicionporIDCliente(oMedicion);

                    Limpiar();
                    Refrescar();
                }
                else{
                    lblMensaje.Text = "ERRROR:"+" No debe de dejar espacios en blanco" + " - Ocurrió algun error con los datos insertados, verifique" + " - La cedula ya fue digitada";
                }
                
            }
            catch (Exception)
            {
                lblMensaje.Text = "ERRROR:" + " No debe de dejar espacios en blanco" + " - Ocurrió algun error con los datos insertados, verifique" + " - La cedula ya fue digitada";
            }
        }

        protected void gvCliente_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            String codigo = gvCliente.DataKeys[e.RowIndex].Value.ToString();
            int id = Convert.ToInt32(codigo);
            GestorTrainer.EliminarMediciones(id);
            GestorTrainer.EliminarCliente(id);
            gvCliente.EditIndex = -1;
            Refrescar();
        }

        private void Limpiar(){
            txtCedula.Text = ""; txtNombre.Text = ""; txtCorreo.Text = ""; txtFechaN.Text = ""; txtTelefono.Text = ""; txtDietaD.Text = "";
            txtPeso.Text = "";
            txtRitmoC.Text = "";
            txtBrazoR.Text = "";
            txtBrazoT.Text = "";
            txtAntebrazo.Text = "";
            txtMuslo.Text = "";
            txtPechoN.Text = "";
            txtPechoI.Text = "";
            txtGemelos.Text = "";
            txtCintura.Text = "";
            txtCadera.Text = "";
            txtPorcentajeG.Text = "";
            txtAltura.Text = "";
            lblIMC2.Text = "";

            lblMensaje.Text = "";

            btnAceptar.Text = "Guardar";
            lblIMC.Visible = false;
            lblIMC2.Visible = false;
        }
    }
}
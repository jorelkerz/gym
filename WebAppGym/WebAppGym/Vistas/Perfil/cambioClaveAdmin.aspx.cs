﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebAppGym.Vistas.Perfil
{
    public partial class cambioClaveAdmin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
                        
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            Response.Redirect("loginTrainers.aspx");
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            lblMensaje.Text = "";
            try
            {
                if (txtPass.Text.Equals(txtPass.Text) && txtPassOld.Text != "" && txtPass.Text != "" &&txtPass2.Text != "" )
                {
                    string nombreDeUsuario = User.Identity.Name;

                    //string nombreDeUsuario2 = Page.User.Identity.Name;
                    MembershipUser user = Membership.GetUser(nombreDeUsuario);
	                user.ChangePassword(txtPassOld.Text, txtPass.Text);

                    btnAceptar.Visible = false; btnCancelar.Visible = false;
                    lblMensaje2.Visible = true;
                    lblMensaje2.Text = "Contraseña actualizada con exito";
                    btnContinuar.Visible = true;
                }
                else
                {
                    lblMensaje.Text = "ERRROR:" + " No debe de dejar espacios en blanco" + " - Ocurrió algun error con los datos insertados, verifique" + " - Las contraseñas no son iguales";
                }
            }
            catch (Exception)
            {
                lblMensaje.Text = "ERRROR:" + " No debe de dejar espacios en blanco" + " - Ocurrió algun error con los datos insertados, verifique" + " - Las contraseñas no son iguales";
            }
        }

        protected void btnContinuar_Click(object sender, EventArgs e)
        {
            Response.Redirect("loginTrainers.aspx");
        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Perfil/perfilTrainerMaster.Master" AutoEventWireup="true" CodeBehind="listaReportesTrainers.aspx.cs" Inherits="WebAppGym.Vistas.Perfil.listaReportesTrainers" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="contenedorDatosSocios ">
        <div class="datosPersonales">
            <p>
                <span>Elija una opcion para seguir:
                    <ul>
                        <li><asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Vistas/Perfil/reporteGraficoDias.aspx">Reporte clientes por dias</asp:HyperLink></li>
                        <li><asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Vistas/Perfil/reporteClientes.aspx">Reporte información clientes</asp:HyperLink></li>
                        <li><asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="~/Vistas/Perfil/reporteRutinasClientes.aspx">Reporte rutinas por cliente</asp:HyperLink></li>
                    </ul>
                </span>
            </p>
        </div>
    </div>
</asp:Content>

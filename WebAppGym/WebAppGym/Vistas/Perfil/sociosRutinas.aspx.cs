﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebAppGym.BLL;
using WebAppGym.Clases;
using WebAppGym.Singleton;

namespace WebAppGym.Vistas.Perfil
{
    public partial class sociosRutinas : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }



        public void cargarRutinasClienteHipertrofia()
        {
            int idCliente = GestorTrainer.idSeccion;
            List<RutinaCliente> listarutinaC = GestorTrainer.obtieneRutinaCliente(idCliente);

            List<Hipertrofia> listaHipertrofia = GestorTrainer.ListaHipertrofia();
            List<Cardio> listaCardio = GestorTrainer.ListaCardio();


            String crearRutina = "";
            bool lunes = true;
            bool martes = true;
            bool miercoles = true;
            bool jueves = true;
            bool viernes = true;
            bool sabado = true;
            bool domingo = true;

            foreach (RutinaCliente rutinaCliente in listarutinaC)
            {
                if (rutinaCliente.idHipertrofia != 0)
                {

                    foreach (Hipertrofia hipertrofia in listaHipertrofia)
                    {
                        if (rutinaCliente.idHipertrofia == hipertrofia.id)
                        {
                            crearRutina += "<div class='diasSemana'>";

                            if (rutinaCliente.dia.Equals("Lunes"))
                            {
                                if (lunes)
                                {
                                    crearRutina += "<p>" + rutinaCliente.dia + " Hipertrofia " + "</p>";
                                    lunes = false;
                                }
                            }
                            if (rutinaCliente.dia.Equals("Martes "))
                            {
                                if (martes)
                                {
                                    crearRutina += "<p>" + rutinaCliente.dia + " Hipertrofia " + "</p>";
                                    martes = false;
                                }
                            }
                            if (rutinaCliente.dia.Equals("Miercoles"))
                            {
                                if (miercoles)
                                {
                                    crearRutina += "<p>" + rutinaCliente.dia + " Hipertrofia " + "</p>";
                                    miercoles = false;
                                }
                            }
                            if (rutinaCliente.dia.Equals("Jueves"))
                            {
                                if (jueves)
                                {
                                    crearRutina += "<p>" + rutinaCliente.dia + " Hipertrofia " + "</p>";
                                    jueves = false;
                                }
                            }
                            if (rutinaCliente.dia.Equals("Viernes"))
                            {
                                if (viernes)
                                {
                                    crearRutina += "<p>" + rutinaCliente.dia + " Hipertrofia " + "</p>";
                                    viernes = false;
                                }
                            }
                            if (rutinaCliente.dia.Equals("Sabados"))
                            {
                                if (sabado)
                                {
                                    crearRutina += "<p>" + rutinaCliente.dia + " Hipertrofia " + "</p>";
                                    sabado = false;
                                }
                            }
                            if (rutinaCliente.dia.Equals("Domingos"))
                            {
                                if (domingo)
                                {
                                    crearRutina += "<p>" + rutinaCliente.dia + " Hipertrofia " + "</p>";
                                    domingo = false;
                                }
                            }
                            crearRutina += "</div>";
                            crearRutina += "<div class='ejercicios'>";
                            crearRutina += "<br>";
                            crearRutina += " <a href='";
                            crearRutina += hipertrofia.url;
                            crearRutina += "'>";
                            crearRutina += hipertrofia.ejercicio;
                            crearRutina += "</a>";
                            crearRutina += "<br>";
                            crearRutina += "</div>";
                        }
                    }
                }
            }
            Response.Write(crearRutina);
        }

        public void cargarRutinasClienteCardio()
        {

            List<RutinaCliente> listarutinaC = GestorTrainer.obtieneRutinaCliente(2585765);


            List<Cardio> listaCardio = GestorTrainer.ListaCardio();


            String crearRutina = "";
            bool lunes = true;
            bool martes = true;
            bool miercoles = true;
            bool jueves = true;
            bool viernes = true;
            bool sabado = true;
            bool domingo = true;

            foreach (RutinaCliente rutinaCliente in listarutinaC)
            {
                if (rutinaCliente.idCardio != 0)
                {
                    foreach (Cardio cardio in listaCardio)
                    {
                        if (rutinaCliente.idCardio == cardio.id)
                        {
                            crearRutina += "<div class='diasSemana'>";

                            if (rutinaCliente.dia.Equals("Lunes"))
                            {
                                if (lunes)
                                {
                                    crearRutina += "<p>" + rutinaCliente.dia + " Cardio " + "</p>";
                                    lunes = false;
                                }
                            }
                            if (rutinaCliente.dia.Equals("Martes"))
                            {
                                if (martes)
                                {
                                    crearRutina += "<p>" + rutinaCliente.dia + " Cardio " + "</p>";
                                    martes = false;
                                }
                            }
                            if (rutinaCliente.dia.Equals("Miercoles"))
                            {
                                if (miercoles)
                                {
                                    crearRutina += "<p>" + rutinaCliente.dia + " Cardio " + "</p>";
                                    miercoles = false;
                                }
                            }
                            if (rutinaCliente.dia.Equals("Jueves"))
                            {
                                if (jueves)
                                {
                                    crearRutina += "<p>" + rutinaCliente.dia + " Cardio " + "</p>";
                                    jueves = false;
                                }
                            }
                            if (rutinaCliente.dia.Equals("Viernes"))
                            {
                                if (viernes)
                                {
                                    crearRutina += "<p>" + rutinaCliente.dia + " Cardio " + "</p>";
                                    viernes = false;
                                }
                            }
                            if (rutinaCliente.dia.Equals("Sabados"))
                            {
                                if (sabado)
                                {
                                    crearRutina += "<p>" + rutinaCliente.dia + " Cardio " + "</p>";
                                    sabado = false;
                                }
                            }
                            if (rutinaCliente.dia.Equals("Domingos"))
                            {
                                if (domingo)
                                {
                                    crearRutina += "<p>" + rutinaCliente.dia +" Cardio " + "</p>";
                                    domingo = false;
                                }
                            }
                            crearRutina += "</div>";
                            crearRutina += "<div class='ejercicios'>";
                            crearRutina += "<br>";
                            crearRutina += " <a href='";
                            crearRutina += cardio.url;
                            crearRutina += "'>";
                            crearRutina += cardio.ejercicio;
                            crearRutina += "</a>";
                            crearRutina += "<br>";
                            crearRutina += "</div>";
                        }
                    }
                }
            }
            Response.Write(crearRutina);
        }


        //crearRutina = "<a href='http://www.w3schools.com'>Visit W3Schools</a> ";








    }
}
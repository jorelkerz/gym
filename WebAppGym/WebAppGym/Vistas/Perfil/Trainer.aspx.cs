﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebAppGym.Clases;
using WebAppGym.Singleton;

namespace WebAppGym.Vistas.Perfil
{
    public partial class Trainer : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ddlCardio.Visible = false;

                ddlCardio.DataSource = GestorTrainer.ListaCardio();
                ddlCardio.DataTextField = "nombre";
                ddlCardio.DataValueField = "id";
                ddlCardio.DataBind();

                ddlHipertrofia.DataSource = GestorTrainer.ListaHipertrofia();
                ddlHipertrofia.DataTextField = "nombre";
                ddlHipertrofia.DataValueField = "id";
                ddlHipertrofia.DataBind();

                ddlClientes.DataSource = GestorTrainer.ListaClientes();
                ddlClientes.DataTextField = "nombre";
                ddlClientes.DataValueField = "id";
                ddlClientes.DataBind();

                Refrescar();
            }
        }

        private void Refrescar()
        {
            gvTrainer.DataSource = GestorTrainer.rutinasTrainer();
            gvTrainer.DataBind();
        }

        protected void rblRutinas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rblRutinas.SelectedValue == "Rutinas Cardio")
            {
                ddlCardio.Visible = true;
                ddlHipertrofia.Visible = false;
            }
            else
            {
                ddlCardio.Visible = false;
                ddlHipertrofia.Visible = true;
            }
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            RutinaCliente trainer = new RutinaCliente();
            trainer.idCliente = Convert.ToInt32(ddlClientes.SelectedValue);
            trainer.dia = ddlDias.SelectedValue;

            if (rblRutinas.SelectedValue == "Rutinas Cardio")
            {
                trainer.idCardio = Convert.ToInt32(ddlCardio.SelectedValue);
                trainer.idHipertrofia = 0;
            }
            else
            {
                trainer.idCardio = 0;
                trainer.idHipertrofia = Convert.ToInt32(ddlHipertrofia.SelectedValue);
            }

            GestorTrainer.InsertarTrainer(trainer);
            //gvTrainer.EditIndex = -1;
            //Refrescar();
            Response.Redirect("Trainer.aspx");
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            ddlClientes.SelectedIndex = -1;
            ddlDias.SelectedIndex = -1;
            rblRutinas.SelectedIndex = 0;
        }

        protected void gvTrainer_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            var colsNoVisible = gvTrainer.DataKeys[e.RowIndex].Values;
            
            RutinaCliente trainer = new RutinaCliente();

            trainer.idCliente = (int)colsNoVisible[0];
            trainer.dia = (string)colsNoVisible[3];
            trainer.idHipertrofia = (int)colsNoVisible[1];
            trainer.idCardio = (int)colsNoVisible[2];

            GestorTrainer.BorrarTrainer(trainer);
            gvTrainer.EditIndex = -1;
            Refrescar();
        }
    }
}
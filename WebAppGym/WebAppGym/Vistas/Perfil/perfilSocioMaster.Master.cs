﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebAppGym.Singleton;

namespace WebAppGym.Vistas.Perfil
{
    public partial class perfilSocioMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (GestorTrainer.idSeccion == -1)
                {
                    Response.Redirect("login.aspx");
                }
            }
            catch {
                Response.Redirect("login.aspx");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Session.Remove("cedula");
            GestorTrainer.idSeccion = -1;
            Response.Redirect("login.aspx");
        }

  
    }
}
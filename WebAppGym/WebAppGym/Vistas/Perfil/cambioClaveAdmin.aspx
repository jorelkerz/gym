﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="cambioClaveAdmin.aspx.cs" Inherits="WebAppGym.Vistas.Perfil.cambioClaveAdmin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Crear Administrador</title>
    <link href="../../css/animate2.css" rel="stylesheet" />
    <link href="../../css/style2.css" rel="stylesheet" />
    <link href="../../css/cssgoogle1.css" rel="stylesheet" />
    <link href="../../css/jlcss.css" rel="stylesheet" />
    <script src="../../js/jquery.min.js"></script>
    <style type="text/css">
        .auto-style1 {
            color: #FF0000;
            font-weight: normal;
        }
        .auto-style2 {
            color: #00FF00;
            font-weight: normal;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div class="container">
                <div class="top">
                    <h1 id="title" class="hidden"><span id="logo">MY GYM</span></h1>
                </div>
                <div class="login-box animated fadeInDown">
                    <div class="datosPersonales">
                        <p><asp:Label ID="lblUser" runat="server" Text=" " AssociatedControlID="LoginName1"></asp:Label>
                            <asp:LoginName ID="LoginName1" runat="server" /></p>
                        <table>
                            <tr>
                                <td><asp:Label ID="lblPassOld" runat="server" Text="Vieja contraseña: "></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="txtPassOld" runat="server" TextMode="Password"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td><asp:Label ID="lblPass" runat="server" Text="Nueva contraseña: "></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="txtPass" runat="server" TextMode="Password"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td><asp:Label ID="lblPass2" runat="server" Text="Repita la nueva contraseña: "></asp:Label></td>
                                <td>
                                    <asp:TextBox ID="txtPass2" runat="server" TextMode="Password"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td colspan ="2">
                                    <asp:Label ID="lblMensaje" runat="server" Text=" " CssClass="auto-style1"></asp:Label>
                                    <asp:Label ID="lblMensaje2" runat="server" Text=" " CssClass="auto-style2" Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan ="2">
                                    <asp:Button ID="btnAceptar" runat="server" Text="Aceptar" OnClick="btnAceptar_Click"/>
                                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" OnClick="btnCancelar_Click"/>
                                    <asp:Button ID="btnContinuar" runat="server" Text="Continuar" Visible="false" OnClick="btnContinuar_Click"/>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>

<script>
    $(document).ready(function () {
        $('#logo').addClass('animated fadeInDown');
        $("input:text:visible:first").focus();
    });
</script>
</html>

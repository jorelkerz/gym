﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Perfil/perfilTrainerMaster.Master" AutoEventWireup="true" CodeBehind="reporteClientes.aspx.cs" Inherits="WebAppGym.Vistas.reporteClientes" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Vistas/Perfil/listaReportesTrainers.aspx">Volver...</asp:HyperLink><br />
    <p>Usuario: 
        <asp:LoginName ID="LoginName1" runat="server" />
    </p>
    <rsweb:ReportViewer ID="ReportViewer1" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%">
        <LocalReport ReportEmbeddedResource="WebAppGym.Vistas.reporteClientes.rdlc" ReportPath="Vistas/Perfil/reporteClientes.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="SeleccionarTodos" TypeName="WebAppGym.BLL.BLL_Cliente, WebAppGym, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null"></asp:ObjectDataSource>
</asp:Content>

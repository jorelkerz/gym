﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebAppGym.Clases;
using WebAppGym.Singleton;

namespace WebAppGym.Vistas.Perfil
{
    public partial class socios : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                cargarPerfil();
            }

        }

        public void cargarPerfil()
        {
            //se obtine los datos personales

            int idCliente = GestorTrainer.idSeccion;
            Cliente cliente = GestorTrainer.ObtenerClienteporID(idCliente);
            lblNombre.Text = cliente.nombre;
            lblCorreo.Text = cliente.email;
            lblTelefono.Text = "Telefono: "+Convert.ToString(cliente.telefono);
            lblfechaNacimiento.Text = "Nacimiento: "+Convert.ToString(cliente.fechaNacimiento.ToString("dd MMMM yyyy"));
            
            //se obtiene las mediciones
            Medicion medicion = GestorTrainer.ObtenerMedicionporIDCliente(cliente.id);
            lblAltura.Text = "Altura: " + Convert.ToString(medicion.altura) + " cm";
            lblPeso.Text = "Peso: "+Convert.ToString(medicion.peso)+" kg";
            lblRitmoCardiaco.Text = "Ritmo Cardio: " + Convert.ToString(medicion.ritmoCardiaco) + " .seg";
            lblBrazoRelajado.Text = "Brazo Relajado: " + Convert.ToString(medicion.brazoRelajado) + " cm";
            lblBrazoTenso.Text = "Brazo Tenso: " + Convert.ToString(medicion.brazoTension) + " cm";
            lblAntebrazo.Text = "AnteBrazo: " + Convert.ToString(medicion.antebrazo) + " cm";
            lblMuslo.Text = "Muslo: " + Convert.ToString(medicion.muslo) + " cm";
            lblPechoNormal.Text = "Pecho Normal: " + Convert.ToString(medicion.pechoNomal) + " cm";
            lblPechoHinchado.Text = "Pecho Hinchado: " + Convert.ToString(medicion.pechoHinchado) + " cm";
            lblGemelos.Text = "Gemelos: " + Convert.ToString(medicion.gemelos) + " cm";
            lblCintura.Text = "Cintura: " + Convert.ToString(medicion.cintura) + " cm";
            lblCadera.Text = "Cadera: " + Convert.ToString(medicion.cadera) + " cm";
            lblPorcentajeGrasa.Text = "Porcentaje Grasa: " + Convert.ToString(medicion.porcentajeGrasa) + " %";
            lblIMC.Text = "IMC: " + Convert.ToString(medicion.IMC);
            lblfechaRegistro.Text = "Ultima Lectura: " + Convert.ToString(medicion.fechaRegistro.ToString("dd MMMM yyyy"));

            //se obtine la dieta
            lbldescripcionDieta.Text = cliente.descripcionDieta;
        }

    }
}
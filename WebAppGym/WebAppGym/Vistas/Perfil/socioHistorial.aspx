﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Perfil/perfilSocioMaster.Master" AutoEventWireup="true" CodeBehind="socioHistorial.aspx.cs" Inherits="WebAppGym.Vistas.Perfil.socioHistorial" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="tablaMantenimientos">

            <asp:TextBox ID="txtCedula" runat="server" Text="" Visible="false"></asp:TextBox>

            <%--<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>--%>
            <rsweb:ReportViewer ID="reportHistorial" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%">
                <LocalReport ReportEmbeddedResource="WebAppGym.Vistas.Perfil.reporteHistorial.rdlc" ReportPath="Vistas/Perfil/reporteHistorial.rdlc">
                    <DataSources>
                        <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
                    </DataSources>
                </LocalReport>
            </rsweb:ReportViewer>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="ObtenerMedicionReporte" TypeName="WebAppGym.BLL.BLL_Medicion, WebAppGym, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null">
                <SelectParameters>
                    <asp:ControlParameter ControlID="txtCedula" PropertyName="Text" DefaultValue=" " Name="idCliente" Type="Int32"></asp:ControlParameter>
                </SelectParameters>
            </asp:ObjectDataSource>
        <%--</div>--%>
    </div>
</asp:Content>

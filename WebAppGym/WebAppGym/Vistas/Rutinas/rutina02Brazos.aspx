﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina02Brazos.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina02Brazos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">


        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
                     new AjaxControlToolkit.Slide("images/rutina02Brazos/01.png", "Flexion alterna de mancuerna con el codo", "Final del Movimiento", "images/rutina02Brazos/01.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="false"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>Sentado, una mancuerna en cada mano cogindas en supinación y el codo apoyado en la cara interna del muslo:
            <ul>
                <li>Inspira y efectuar una flexión del codo.</li>
                <li>Espirar al final del esfuerzo.</li>
            </ul>
            Este ejercicio de aislamiento permite el control del movimiento en amplitud, rectitud y velocidad.
            <br /><br />
            Trabaja principalmente el bíceps y el braquial anterior. 
        </span>
    </div>

</asp:Content>

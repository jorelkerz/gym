﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina07Brazos.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina07Brazos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">


        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
                     new AjaxControlToolkit.Slide("images/rutina07Brazos/01.png", "Flexión de codos con barra, manos en supinación", "Ejecución del movimiento", "images/rutina07Brazos/01.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="false"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>De pie,con la espalda bien recta, la barra cogida con las manos en supinación con una separación ligeramente mayor que la anchura de los hombros:
            <ul>
                <li>Inspira y flexionar los codos procurando no flexioanr el tronco,mediante una contraccion isometrica de los músculos glúteos, espinales y abdominales.</li>
                <li>Espirar al final del movimiento.</li>
            </ul>
            Este ejercicio solicita bíceps braquial, el braquial anterior, el braquiorradial, el pronador redondo y el conjunto de flexores de la muñeca y dedos. 
        </span>
    </div>

</asp:Content>

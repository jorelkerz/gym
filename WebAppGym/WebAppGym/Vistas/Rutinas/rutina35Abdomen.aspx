﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina35Abdomen.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina35Abdomen" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">


        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
                     new AjaxControlToolkit.Slide("images/rutina35Abdomen/01.png", "Elevaciones de piernas en plancha inclinada", "Variante del Movimiento", "images/rutina35Abdomen/01.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="false"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>Estirado sobre la plancha inclianda, manos agarradas de los barrotes:
            <ul>
                <li>Elevar hasta la horizontal.</li>
                <li>Luego separar la pelvis enrollando la columna vertebral para intentar tocar la cabeza con las rodillas.</li>
            </ul>
            Este ejercicio trabaja en la primera fase, durante la elevación de las piernas, el psoas iliaco, el tensor de la fascia lata y el recto femoral del cuadriceps.
            <br /><br />
            En la segunda fase durante la eleveación de la pelvis y la flexión de la columna, se solicita a cincha abdominal, la parte de los rectos mayores del abdmone. 
        </span>
    </div>

</asp:Content>

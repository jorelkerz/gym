﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina12Brazos.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina12Brazos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">

        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
                     new AjaxControlToolkit.Slide("images/rutina12Brazos/01.png", "Curl de bíceps con barra y agarre en pronación, presa martillo", "Ejecución del Movimiento", "images/rutina12Brazos/01.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="false"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>De pie, piernas ligeramente separadas, brazos extendidos, manos en pronación:
            <ul>
                <li>Inspira y flexionar los codos, espirar al final del movimiento.</li>
                <li>Volver a la posición inicial, controlando el descenso de la barra.</li>
            </ul>
            Este ejercicio permite trabajar los extensores de las muñecas: extensor radial largo y corto del carpo, extensor común de los dedos, extensor propio del meñique y extensor cubital del carpo.
            <br /><br />
            Además, su acción se extendie al braquiorradial, el braquial anterior y en menor medida el bíceps braquial.  
        </span>
    </div>

</asp:Content>

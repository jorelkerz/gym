﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutinaCuerda.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutinaCuerda" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

        <div class="imagenesRutina">


        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
                     new AjaxControlToolkit.Slide("images/cuerda/cuerda.jpg", "Cuerda", "Salto de cuerda", "images/cuerda/cuerda.jpg")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="false"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span><h2>Técnica básica para saltar la cuerda</h2>
            <ul>
                <li>Comienza por escoger una cuerda que no sea ni demasiado larga, ni demasiado corta. Para seleccionar la cuerda adecuada, pisa el centro de la cuerda y jala las agarraderas hacia arriba asegurándote que éstas lleguen a la altura de tus hombros. Inicia parándote sobre tus pies juntos, y manteniendo el torso erguido. Tus codos deben doblarse formando un ángulo de 45 grados y debes mantenerlos pegados a tu cuerpo. Usa tus muñecas y no tus brazos para hacer girar la cuerda. Eleva las puntas de tus pies sólo lo suficiente para que pase la cuerda y puedas aterrizar amortiguando con tus rodillas.</li>
            </ul>
      </span>
    </div>
</asp:Content>

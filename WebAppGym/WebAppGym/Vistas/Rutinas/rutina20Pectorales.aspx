﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina20Pectorales.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina20Pectorales" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">

        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
                     new AjaxControlToolkit.Slide("images/rutina20Pectorales/01.png", "Press de banco inclinado", "Ejecución del Movimiento", "images/rutina20Pectorales/01.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="false"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>Acostado, cabeza hacia abajo, en un banco mas o menos incliando entre 20° y 40°, pies fijos para evitar deslizamientos, así la barra con manos en pronación y speradas en una distancia igual o superior a la anchura de los hombros:
            <ul>
                <li>Inspirar y bajar la barra sobre la parte baja de los pectorales controlando el movimiento.</li>
                <li>Desarrolar espirando al final del esfuerzo.</li>
            </ul>
            Este ejercicio solicita el pectoral, principalmente los haces inferiores, los triceps y los haces anteriores del deltoides, bajando la barra a nivel del cuello, con cargas ligeras se elonga el pectoral mayor estirandolo favorablemente.  
        </span>
    </div>

</asp:Content>

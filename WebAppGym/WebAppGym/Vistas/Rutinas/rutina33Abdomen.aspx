﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina33Abdomen.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina33Abdomen" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">


        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
            new AjaxControlToolkit.Slide("images/rutina33Abdomen/01.png", "Encogimientos Abdominales o Crunch", "Ejecución del Movimiento 1", "images/rutina33Abdomen/01.png"),
            new AjaxControlToolkit.Slide("images/rutina33Abdomen/02.png", "Encogimientos Abdominales o Crunch", "Ejecución del Movimiento 2", "images/rutina33Abdomen/02.png"),
            new AjaxControlToolkit.Slide("images/rutina33Abdomen/03.png", "Encogimientos Abdominales o Crunch", "Variante del Movimiento", "images/rutina33Abdomen/03.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:Button runat="Server" ID="prevButton" Text="Prev" Font-Size="Larger" />
            <asp:Button runat="Server" ID="playButton" Text="Play" Font-Size="Larger" />
            <asp:Button runat="Server" ID="nextButton" Text="Next" Font-Size="Larger" />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="true"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                NextButtonID="nextButton"
                PlayButtonText="Play"
                StopButtonText="Stop"
                PreviousButtonID="prevButton"
                PlayButtonID="playButton"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>Acostado boca arriba, manos detrás de la cabeza, muslos en la vertical, rodillas flexionadas:
            <ul>
                <li>Inspira y separar los hombros del suelo acercando la cabeza a las rodillas mediante una incurvación de la columna.</li>
                <li>Espirar al final del movimiento.</li>
            </ul>
            Este ejercicio solicita pirincipalmente el recto mayor del abdomen, para solicitar mas intensamente los oblicuos, basta con acercar alternativamente encogiendo los abdominales, el codo derecho a la rodilla izquierda y el codo izquierdo a la rodilla derecha. 
        </span>
    </div>

</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina21Pectorales.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutinas21Pectorales" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="imagenesRutina">

        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
                     new AjaxControlToolkit.Slide("images/rutina21Pectorales/01.png", "Aperturas con mancuernas en banco plano", "Ejecución del Movimiento", "images/rutina21Pectorales/01.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="false"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>Estirando sobre un banco estrecho para no molestar los mivimientos de los hombros, mancuernas cogidas con las manos, brazos extendidos o codos ligeramente flexionados para descargar la articulacion:
            <ul>
                <li>Inspirar y despues separar los brazos hasta la horizontal.</li>
                <li>Elevar los brazos hasta la vetical espirando al mismo tiempo.</li>
                <li>Provocar una pequeña contracción isometrica al final del movimiento.</li>
            </ul>
            Constituye un ejercicio básico para aumentar la expansión toráxica que contribuye a aumentar la capacidad pulmonar, excelente para la flexibilidad muscular.  
        </span>
    </div>

</asp:Content>

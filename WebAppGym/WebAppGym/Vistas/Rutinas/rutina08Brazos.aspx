﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina08Brazos.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina08Brazos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">


        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
            new AjaxControlToolkit.Slide("images/rutina08Brazos/01.png", "Bíceps en el banco scott", "Principio del movimiento", "images/rutina08Brazos/01.png"),
            new AjaxControlToolkit.Slide("images/rutina08Brazos/02.png", "Bíceps en el banco scott", "Variante", "images/rutina08Brazos/02.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:Button runat="Server" ID="prevButton" Text="Prev" Font-Size="Larger" />
            <asp:Button runat="Server" ID="playButton" Text="Play" Font-Size="Larger" />
            <asp:Button runat="Server" ID="nextButton" Text="Next" Font-Size="Larger" />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="true"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                NextButtonID="nextButton"
                PlayButtonText="Play"
                StopButtonText="Stop"
                PreviousButtonID="prevButton"
                PlayButtonID="playButton"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>Sentado sobre la máquina, la barra cogida con las manos en supinación, brazos estirados con los codos apoyados sobre el pupitre:
            <ul>
                <li>Inspira y flexiona los codos.</li>
                <li>Espirar al final del movimiento.</li>
            </ul>
            Este ejercicio es uno de los mejores para el bíceps braquial, como los brazos estan apoyados no es posible hacer trampa, se debe de calentar los músculas por la tensión que genera al iniciar, también trabaja el branquial anterior, el braquiorradial, en menor medida, y el pronador redondo.
        </span>
    </div>

</asp:Content>

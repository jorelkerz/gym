﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina25Espalda.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina25Espalda" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">

        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
                     new AjaxControlToolkit.Slide("images/rutina25Espalda/01.png", "Tracción tras nuca en polea alta", "Ejecución del Movimiento", "images/rutina25Espalda/01.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="false"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>Sentado frente al aparato,muslos fijados bajo los cojines, barra asida en pronación, manos muy separadas:
            <ul>
                <li>Inspirar y tirar dela barra hasta la nuca dirigiendo los codos hacia el tronco.</li>
                <li>Espirar al final del movimiento.</li>
            </ul>
            Este ejercicio, excelente para desarrollar la espalda en anchura, trabaja los dorsales anchos y redondos mayores.También son solicitados los flexores de los antebrazos, así como los romboides y parte inferior de los trapecios que actúan en el acercamiento de los omoplatos.  
        </span>
    </div>

</asp:Content>

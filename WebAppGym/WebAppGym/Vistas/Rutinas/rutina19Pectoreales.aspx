﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina19Pectoreales.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina19Pectoreales" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">

        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
                     new AjaxControlToolkit.Slide("images/rutina19Pectorales/01.png", "Press de banco plano", "Ejecución del Movimiento", "images/rutina19Pectorales/01.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="false"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>Acostado sobre un banco plano, glúteos en contacto con el banco, pies en el suelo:
            <ul>
                <li>Coger la barra, manos en pronación y separadas a una longitud superior a la anchura de los hombros.</li>
                <li>Desarrolar espirando al final del esfuerzo.</li>
            </ul>
            Este ejercicio solicita el pectoral mayor en todo su conjunto, el pectoral menos, los triceps, el deltoides anterior, los serratos y el coracobraquial.  
        </span>
    </div>

</asp:Content>

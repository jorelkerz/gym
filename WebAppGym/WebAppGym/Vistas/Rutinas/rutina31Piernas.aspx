﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina31Piernas.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina31Piernas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">

        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
                     new AjaxControlToolkit.Slide("images/rutina31Piernas/01.png", "Isquiotibiales, en máquina o Leg Curl", "Ejecución del Movimiento", "images/rutina31Piernas/01.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="false"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>Estiradoboca abajo en la máquina, manos en los puños, piernas extendidas, tobillos anclados en los cojines:
            <ul>
               <li>Inspirar y efectuar una flexión simultanea de piernas, intentando tocar los glúteos con los talones. Espirar al final del esfuerzo.</li>
               <li>Volver a la posicón inicil controlando el movimiento.</li>
            </ul>
            Este ejercicio trabaja el conjunto de los músculos isquiotibiales, asi como los gemelos y en profundidad, el músculo popliteo Durante la flexión se puede localizar el trabajo bien sobre el semitendinoso y el semimembranoso efectuando una rotación interna de los pies, bien sobre el bíceps femoral, cabeza larga y corta, efectuando una rotación externa de los pies.
        </span>
    </div>

</asp:Content>

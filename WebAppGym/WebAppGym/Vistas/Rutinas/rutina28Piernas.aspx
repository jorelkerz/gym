﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina28Piernas.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina28Piernas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">

        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
                     new AjaxControlToolkit.Slide("images/rutina28Piernas/01.png", "El squat o sentadillas", "Ejecución del Movimiento", "images/rutina28Piernas/01.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="false"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>Solicita una gran parte del sistema muscular. Permite adquirir una buena expasión tóracica y por lo tanto, una buena capacidad respiratoria:
            <ul>
                <li>Barra colocada en el soporte, deslizarse por debajo y situarla sobre los trapecios un poco más alta que los deltoides posteriores,coger la barra con las manos con una separación.</li>
                <li>Inspirar profundamente, arquear ligeramente la zona lumbar efectuando una anteversión pélvica, contraer la cincha abdominal, mirar recto hacia adelante y despegar la barra del soporte.</li>
                <li>Retroceder uno o dos paosos, detenerse con los pies paralelos a una distancia igual a la anchura de los hombros, agacharse inclinando la espalda hacia adelante controlando la baja y sin curvar la columna vertebral.</li>
            </ul>
        </span>
    </div>

</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina16Hombros.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina16Hombros" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">
        
        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
                     new AjaxControlToolkit.Slide("images/rutina16Hombros/01.png", "Elevaciones laterales, tronco inclinado hacia adelante", "Fin del Movimiento", "images/rutina16Hombros/01.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="false"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>De pie, piernas separadas, ligeramente separadas flexionadas, tronco inclianado hacia adelante, manteniendo la espalda recta, brazos colgando, mancuernas en las manos, codos ligeramente flexionados:
            <ul>
                <li>Inspira y elevar los brazos hasta la horizontal.</li>
                <li>Espirar al final del movimiento.</li>
            </ul>
            Este ejercicio trabaja el conjunto de los hombros acentuado el trabajo sobre el deltoides posterior.  
        </span>
    </div>

</asp:Content>

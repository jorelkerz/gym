﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina15Hombros.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina15Hombros" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">


        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
            new AjaxControlToolkit.Slide("images/rutina15Hombros/01.png", "Press sentado con mancuernas", "Variante del Movimiento", "images/rutina15Hombros/01.png"),
            new AjaxControlToolkit.Slide("images/rutina15Hombros/02.png", "Press senatdo con mancuernas", "Fin del Movimiento", "images/rutina15Hombros/02.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:Button runat="Server" ID="prevButton" Text="Prev" Font-Size="Larger" />
            <asp:Button runat="Server" ID="playButton" Text="Play" Font-Size="Larger" />
            <asp:Button runat="Server" ID="nextButton" Text="Next" Font-Size="Larger" />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="true"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                NextButtonID="nextButton"
                PlayButtonText="Play"
                StopButtonText="Stop"
                PreviousButtonID="prevButton"
                PlayButtonID="playButton"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>Sentado, en una banco, con la espalda bien recta, las mancuernas a la altura de los hombros cogidas en pronación:
            <ul>
                <li>Inspira y desarrollar hasta estirar los brazos verticalmente.</li>
                <li>Espirar al final del movimiento.</li>
            </ul>
            Este ejercicio solicita el deltoides, pricipalmente su porción media, asi como el trapecio, el serrato anterior y el triceps braquial.
        </span>
    </div>

</asp:Content>

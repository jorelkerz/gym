﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina22Pectorales.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina22Pectorales" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">

        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
                     new AjaxControlToolkit.Slide("images/rutina22Pectorales/01.png", "Pull-Over con barra en banco plano", "Ejecución del Movimiento", "images/rutina22Pectorales/01.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="false"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>Brazos extendidos, la barra cogida en pronación, manos separadas en una distancia igual a la anchura de los hombros:
            <ul>
                <li>Inspirar ensanchando el pecho al máximo y bajar por detras de la cabeza flexionando ligeramente los codos.</li>
                <li>Espirar al volver a la posición de partida.</li>
            </ul>
            Este ejercicio desarrolla el pectoral mayor, la cabeza larga del triceps, el redondo mayor, el dorsal ancho y los serratos anteriores, el romboides y el pectoral menor.  
        </span>
    </div>

</asp:Content>

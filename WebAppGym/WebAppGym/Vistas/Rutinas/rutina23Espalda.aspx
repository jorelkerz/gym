﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina23Espalda.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina23Espalda" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="imagenesRutina">

        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
                     new AjaxControlToolkit.Slide("images/rutina23Espalda/01.png", "Tracción o dominadas en barra fija", "Ejecución del Movimiento", "images/rutina23Espalda/01.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="false"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>En suspensión en la barra, manos muy separadas en pronación:
            <ul>
                <li>Espirar al final del movimiento.</li>
                <li>Regresar a la posición departida controlando el descenso y volver a empezar.</li>
            </ul>
            Este ejercicio, que demanda cierta fuerza, es excelente para desarrollar el dorso ancho, el redondo mayor y, cuando se acercan los omoplatos al final de la tracción, los romboides.  
        </span>
    </div>

</asp:Content>

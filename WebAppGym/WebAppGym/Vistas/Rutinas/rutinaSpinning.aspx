﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutinaSpinning.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutinaSpinning" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">


        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
 
            new AjaxControlToolkit.Slide("images/spinning/Spinning01.jpg", "Spinning", "Sentado",                "images/spinning/Spinning01.jpg"),
            new AjaxControlToolkit.Slide("images/spinning/Spinning02.jpg", "Spinning", "Semi Sentado",           "images/spinning/Spinning02.jpg"),
           new AjaxControlToolkit.Slide("images/spinning/Spinning03.jgp", "Spinning", "De pie directo",         "images/spinning/Spinning03.jpg"),
            new AjaxControlToolkit.Slide("images/spinning/Spinning04.jgp", "Spinning", "De pie hacia adelante",  "images/spinning/Spinning04.jpg")
                };
            }
        </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:Button runat="Server" ID="prevButton" Text="Prev" Font-Size="Larger" />
            <asp:Button runat="Server" ID="playButton" Text="Play" Font-Size="Larger" />
            <asp:Button runat="Server" ID="nextButton" Text="Next" Font-Size="Larger" />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="true"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                NextButtonID="nextButton"
                PlayButtonText="Play"
                StopButtonText="Stop"
                PreviousButtonID="prevButton"
                PlayButtonID="playButton"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>
            <h2>Sentado</h2>
            <ul>
                <li>Es la postura básica, en la que estás sentada en el asiento y realizas el pedaleo con la fuerza de los músculos de las piernas y de la cadera. Es importante tener la espalda receta.
                </li>
            </ul>
        </span>

        <span>
            <h2>Semi Sentado </h2>
            <ul>
                <li>Manteniendo la postura anterior pero sin que los gluteos toquen el asiento, ligeramente en el aire y parte de la fuerza para aguantar la postura recae sobre los brazos. Así como la gravedad te ayudará a la hora de superar una resistencia mayor, pero como tiene que vencer parte del peso en el movimiento hacia arriba, esta postura es más cansada que la anterior.
                </li>
            </ul>
        </span>

        <span>
            <h2>De pie recto </h2>
            <ul>
                <li>En esta estás de pie, con la espalda recta y perpendicular al suelo. Tus brazos sujetan el manubrio, pero sobre ellos recae poca fuerza. Ésta es la postura en la que más fuerza de resistencia podrás superar, también la más cansada y por supuesto no conseguirás una frecuencia alta durante mucho tiempo.
                </li>
            </ul>
        </span>

        <span>
            <h2>De pie hacia adelante</h2>
            <ul>
                <li>Aunque estás de pie, el peso es soportado por el manubrio a través de los brazos, esta postura inclinada te permite hacer mucha fuerza. Pero puede cambiar mucho según como te agarres para hacer más fuerza en los pedales o para soportar parte del peso y que este no recaiga sobre tus piernas.

                </li>
            </ul>
        </span>
    </div>



</asp:Content>

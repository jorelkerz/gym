﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina37Abdomen.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina37Abdomen" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">


        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
                     new AjaxControlToolkit.Slide("images/rutina37Abdomen/01.png", "Flexión lateral del tronco con mancuerna", "Variante con polea baja", "images/rutina37Abdomen/01.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="false"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>De pie, piernas ligeramente separadas, una mano detrás de la cabeza y la otra sujetando una mancuerna:
            <ul>
                <li>Efectuar una flexión lateral del tronco del lado opuesto a la mancuerna.</li>
                <li>Volver a la posición inicial o sobrepasada efectuando una flexión pasiva a la mancuerna.</li>
                <li>Alternar las series cambiando la mancuerna de lado sin tiempo de recuperación.</li>
            </ul>
            Este ejercicio trabaja los oblicuos, especialmente del lado de la flexión y con menor intensidad el recto mayor y los musclos profundos de la espalda y el cuadrado lumbar.
        </span>
    </div>

</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina03Brazos.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina03Brazos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">


        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
                     new AjaxControlToolkit.Slide("images/rutina03Brazos/01.png", "Flexión de codos con mancuerna, banco inclinado", "Inicio del Movimiento", "images/rutina03Brazos/01.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="false"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>Sentado en un banco inclinado, la espalda pegada al respaldar, con una mancuerna en cada mano en semipronación:
            <ul>
                <li>Inspira y flexionar los codos efectuando una rotación de las muñeas hacia el exterior antes de que los antebrazos alcancen la horizontal, para terminar la flexion en supinación.</li>
                <li>Espirar al final del movimiento.</li>
            </ul>
            Este ejercicio se utuliza para trabajar de forma especial la cabeza larga del bíceps (parte externa), estirada al principio de la flexión de los antebrazos por la posició inclinada del tronco.
            <br /><br />
            Este movimiento tambien trabaja el braquiorradial y el braquial. 
        </span>
    </div>

</asp:Content>

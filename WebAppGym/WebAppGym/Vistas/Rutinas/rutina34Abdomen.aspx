﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina34Abdomen.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina34Abdomen" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">


        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
            new AjaxControlToolkit.Slide("images/rutina34Abdomen/01.png", "Elevaciones de tronco en el suelo", "Ejecución del Movimiento", "images/rutina34Abdomen/01.png"),
            new AjaxControlToolkit.Slide("images/rutina34Abdomen/02.png", "Elevaciones de tronco en el suelo", "Variante del Movimiento en banco inclinado", "images/rutina34Abdomen/02.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:Button runat="Server" ID="prevButton" Text="Prev" Font-Size="Larger" />
            <asp:Button runat="Server" ID="playButton" Text="Play" Font-Size="Larger" />
            <asp:Button runat="Server" ID="nextButton" Text="Next" Font-Size="Larger" />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="true"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                NextButtonID="nextButton"
                PlayButtonText="Play"
                StopButtonText="Stop"
                PreviousButtonID="prevButton"
                PlayButtonID="playButton"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>Aostado boca arriba, rodillas flexionadas,pies en el suelo, manos detras de la cabeza:
            <ul>
                <li>Inspira y elevar el tronco incurvando la espalda.</li>
                <li>Espirar al final del movimiento. Regresar a la posición inicial pero esta vez sin apoyar el tronco en el suelo.</li>
                <li>Volver a emepzar hasta que aparezca una sensación de quemazón en el abdomen.</li>
            </ul>
            Este ejercicio trabaja los flexores de la cadera y los oblicuos pero su acción se centra en el recto mayor del abdomen. 
        </span>
    </div>

</asp:Content>

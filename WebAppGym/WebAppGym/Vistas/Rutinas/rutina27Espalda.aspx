﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina27Espalda.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.images.rutina27Espalda" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">

        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
                     new AjaxControlToolkit.Slide("images/rutina27Espalda/01.png", "Remo horizontal a una mano con mancuernas", "Fin del Movimiento", "images/rutina27Espalda/01.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="false"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>La mancuerna cogida con una mano en semipronación, mano y rodilla opuestas apoyadas sobre un banco:
            <ul>
                <li>Espalda fija, inspirar y tirar de la mancuerna lo mas alto posible, con el brazo paralelo al cuerpo, llevando el coodo bien hacia atrás.</li>
                <li>Espirar al final del movimiento.</li>
            </ul>
            Este ejercicio trabaja principalmente el dorsal ancho, el redondo mayor, el haz posterior del deltoides y, al final de la contracción, el trapecio y el rombiodes, los flexores del brazo, bíceps braquial, braquial y el braquiorradial, también son solicitados.  
        </span>
    </div>

</asp:Content>

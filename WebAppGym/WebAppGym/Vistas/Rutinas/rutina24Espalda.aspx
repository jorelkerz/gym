﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina24Espalda.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina24Espalda" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="imagenesRutina">

        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
                     new AjaxControlToolkit.Slide("images/rutina24Espalda/01.png", "Tracción pecho en polea alta", "Variante del Movimiento, manos en semipronación", "images/rutina24Espalda/01.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:SlideShowExtender ID="SlideShowExtender2" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="false"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>Sentado frente al aparato, piernas fijas, barra cogida en pronación, manos muy separadas:
            <ul>
                <li>Inspirar y tirar dela barra hasta la horquilla esternal, sacando pecho y llevando los codos hacia atras.</li>
                <li>Espirar al final del movimiento.</li>
            </ul>
            Este ejercicio, excelente para desarrollar la espalda en grosor, trabaja las fibras superiores y centrales del dorsal ancho, tambie´n el trapecio, romboides, el bíceps braquial y en menor medida los pectorales.  
        </span>
    </div>

</asp:Content>

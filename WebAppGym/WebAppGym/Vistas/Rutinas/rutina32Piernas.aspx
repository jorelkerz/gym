﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina32Piernas.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina32Piernas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">


        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
            new AjaxControlToolkit.Slide("images/rutina32Piernas/01.png", "Extensión de tobillos en la máquina", "Ejecución del Movimiento", "images/rutina32Piernas/01.png"),
            new AjaxControlToolkit.Slide("images/rutina32Piernas/02.png", "Extensión de tobillos en la máquina", "Variante en máquina frontal", "images/rutina32Piernas/02.png"),
            new AjaxControlToolkit.Slide("images/rutina32Piernas/03.png", "Extensión de tobillos en la máquina", "Variante con una barra libre", "images/rutina32Piernas/03.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:Button runat="Server" ID="prevButton" Text="Prev" Font-Size="Larger" />
            <asp:Button runat="Server" ID="playButton" Text="Play" Font-Size="Larger" />
            <asp:Button runat="Server" ID="nextButton" Text="Next" Font-Size="Larger" />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="true"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                NextButtonID="nextButton"
                PlayButtonText="Play"
                StopButtonText="Stop"
                PreviousButtonID="prevButton"
                PlayButtonID="playButton"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>De pie, la espalda bien recta, hombros bajo las partes fórradas del aparato, la punta de los pies sobre la calza, los tobillos en flexión pasiva:
            <ul>
                <li>Efectuar una extensión de los pies siempre manteniendo la articulación de las rodillas en extensión.</li>
             </ul>
            Este ejercicio solicita el triceps sural, en importante efectuar en cada repetición una flexión completa para estirar bien los músculos.
        </span>
    </div>

</asp:Content>

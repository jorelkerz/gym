﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina29Piernas.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina29Piernas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">

        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
                     new AjaxControlToolkit.Slide("images/rutina29Piernas/01.png", "Press de piernas inclinado", "Ejecución del Movimiento", "images/rutina29Piernas/01.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="false"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>Colocado sobre el aparato, espalda bien apoyada en el respaldo, pies medianamente separados:
            <ul>
               <li>Inspirar, desbloquear la seguridad y flexionar las rodillas al maximo hasta llevar los muslos a los lados de la caja tóracica, volver a la posicion inicial, espirando al final del movimiento.</li>
            </ul>
            Si se situan los pies en la parte baja de la plataforma se solicitan mas los cuadriceps, si, por el contrario, se colocan los pies en la parte alta de la plataforma, el esfuerzo se desplazará sobre los glúteos y los isquiotibiales. Si los pies estan separados, el esfuerzo se da en los aductores.
        </span>
    </div>

</asp:Content>

﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vistas/Rutinas/rutinasMaster.Master" AutoEventWireup="true" CodeBehind="rutina26Espalda.aspx.cs" Inherits="WebAppGym.Vistas.Rutinas.rutina26Espalda" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <div class="imagenesRutina">

        <script runat="Server" type="text/C#">
            [System.Web.Services.WebMethod]
            [System.Web.Script.Services.ScriptMethod]
            public static AjaxControlToolkit.Slide[] GetSlides()
            {
                return new AjaxControlToolkit.Slide[] { 
                     new AjaxControlToolkit.Slide("images/rutina26Espalda/01.png", "Remo en polea baja, manos en semipronación", "Ejecución del Movimiento", "images/rutina26Espalda/01.png")};
            }
    </script>
        <div style="text-align: center">
            <asp:Label runat="Server" ID="imageTitle" class="slideTitle" /><br />
            <asp:Image ID="Image1" runat="server"
                Height="300"
                Width="200"
                Style="border: 1px solid black;" />
            <asp:Label runat="server" ID="imageDescription" class="slideDescription"></asp:Label><br />
            <br />
            <asp:SlideShowExtender ID="SlideShowExtender1" runat="server"
                TargetControlID="Image1"
                SlideShowServiceMethod="GetSlides"
                AutoPlay="false"
                ImageTitleLabelID="imageTitle"
                ImageDescriptionLabelID="imageDescription"
                Loop="true" SlideShowAnimationType="SlideRight" />
        </div>
    </div>

    <div class="descripcionRutina">
        <span>Sentado frente al aparato, pies anclados, tronco flexionado:
            <ul>
                <li>Inspirar y llevar el mango hasta la base del esternón enderezando la espalda y tirando los codos hacia atrás lo mas lejos posible.</li>
                <li>Espirar al final del movimiento y regresar suavemente a la posición inicial en un movimiento progresivo.</li>
            </ul>
            Este ejercicio es excelente para trabajar la espalda en grosor, localiza el esfuerzo sobre el dorsal ancho, el redondo mayor, el haz posterior del deltoides, el bíceps braquial, el braquiorradial y al final del movimiento sobre el trapecio y el romboides.  
        </span>
    </div>

</asp:Content>

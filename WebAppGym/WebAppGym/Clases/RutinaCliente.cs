﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppGym.Clases
{
    public class RutinaCliente
    {

        public int id { get; set; }
        public int idCliente { get; set; }
        public String dia { get; set; }
        public int idHipertrofia { get; set; }
        public int idCardio { get; set; }

        public string nombreCli { set; get; }
        public string nombreEjer { set; get; }

        public RutinaCliente() { }

        public RutinaCliente(int id, String dia, int idCliente, int idHipertrofia, int idCardio, string nombreCli,
            string nombreEjer) {

            this.id = id;
            this.idCliente = idCliente;
            this.dia = dia;
            this.idHipertrofia = idHipertrofia;
            this.idCardio = idCardio;
            this.nombreCli = nombreCli;
            this.nombreEjer = nombreEjer;
        }
    }
}
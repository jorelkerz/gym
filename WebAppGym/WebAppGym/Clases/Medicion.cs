﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppGym.Clases
{
    public class Medicion
    {
        public int id { get; set; }
        public int altura { set; get; }
        public DateTime fechaRegistro { get; set; }
        public double peso { get; set; }
        public double ritmoCardiaco { get; set; }
        public double brazoRelajado { get; set; }
        public double brazoTension { get; set; }
        public double antebrazo { get; set; }
        public double muslo { get; set; }
        public double pechoNomal { get; set; }
        public double pechoHinchado { get; set; }
        public double gemelos { get; set; }
        public double cintura { get; set; }
        public double cadera { get; set; }
        public double porcentajeGrasa { get; set; }
        public double IMC { get; set; }




    }//fin class
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppGym.Clases
{
    public class Empleado : Persona
    {

        public String perfil { get; set; }


        public Empleado(int id, String nombre, DateTime fechaNacimiento, DateTime fechaRegistro,String genero, int telefono,
            String contrasena, String email, String perfil)
        {
            this.id = id;
            this.nombre = nombre;
            this.fechaNacimiento = fechaNacimiento;
            this.fechaRegistro = fechaRegistro;
            this.genero = genero;
            this.telefono = telefono;
            this.contrasena = contrasena;
            this.email = email;
            this.perfil = perfil;
        }





    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppGym.Clases
{
    public class urlRutinas
    {
        public String nombre { get; set; }
        public String direccion { get; set; }


        public urlRutinas(String nombre, String direccion)
        {
            this.nombre = nombre;
            this.direccion = direccion;
        }

        public urlRutinas() { }
    }
}
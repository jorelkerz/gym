﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppGym.Clases
{
    public class Hipertrofia : Rutina
    {

        public String serie { get; set; }
        public String repeticion { get; set; }
        public String modo { get; set; }
   


        public Hipertrofia(int id, String nombre,String ejercicio, String url, String descanso, String calorias, String serie, String repeticion, String modo)
        {
            this.id = id;
            this.nombre = nombre;
            this.ejercicio = ejercicio;
            this.url = url;
            this.descanso = descanso;
            this.calorias = calorias;
            this.serie = serie;
            this.repeticion = repeticion;
            this.modo = modo;
            this.calorias = calorias;
        }

        public Hipertrofia(int id, String nombre, String descanso, String calorias, String serie, String repeticion, String modo)
        {
            this.id = id;
            this.nombre = nombre;
            this.descanso = descanso;
            this.calorias = calorias;
            this.serie = serie;
            this.repeticion = repeticion;
            this.modo = modo;
        }

        public Hipertrofia(String nombre, String descanso, String calorias, String serie, String repeticion, String modo)
        {
            this.nombre = nombre;
            this.descanso = descanso;
            this.calorias = calorias;
            this.serie = serie;
            this.repeticion = repeticion;
            this.modo = modo;
        }
        public Hipertrofia() { }

    }
}
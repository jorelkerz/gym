﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppGym.Clases
{
    public abstract class Persona
    {

        public int id { get; set; }
        public String nombre { get; set; }
        public DateTime fechaNacimiento { get; set; }
        public DateTime fechaRegistro { get; set; }
        public String genero { get; set; }
        public int telefono { get; set; }
        public String contrasena { get; set; }
        public String email { get; set; }
        public String estado { get; set; }
        public string descripcionDieta { get; set; }

    }
}
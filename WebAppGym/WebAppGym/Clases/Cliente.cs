﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppGym.Clases
{
    public class Cliente : Persona
    {


        public Cliente(int id, String nombre, DateTime fechaNacimiento, DateTime fechaRegistro, String genero,
            int telefono, String contrasena, String email, String codigoActivador,
            String estado, string descripcionDieta)
        {
            this.id = id;
            this.nombre = nombre;
            this.fechaNacimiento = fechaNacimiento;
            this.fechaRegistro = fechaRegistro;
            this.genero = genero;
            this.contrasena = contrasena;
            this.telefono = telefono;
            this.email = email;
            this.estado = estado;
            this.descripcionDieta = descripcionDieta;

        }

        public Cliente() { }


    }
}
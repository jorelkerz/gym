﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppGym.Clases
{
    public abstract class Rutina
    {
        public int id { get; set; }
        public String ejercicio { get; set; }
        public String url { get; set; }
        public String nombre { get; set; }
        public String descanso { get; set; }
        public String calorias { get; set; }
        public String estado { get; set; }

    }
}
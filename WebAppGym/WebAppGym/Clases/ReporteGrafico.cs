﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppGym.Clases
{
    public class ReporteGrafico
    {
        public int cantidad { get; set; }
        public String dia { get; set; }

        public ReporteGrafico(int cantidad, String dia)
        {
            this.cantidad = cantidad;
            this.dia = dia;
        }

        public ReporteGrafico() { }
    }

}
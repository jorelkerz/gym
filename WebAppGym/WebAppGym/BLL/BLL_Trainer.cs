﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using WebAppGym.Clases;
using WebAppGym.DAL;

namespace WebAppGym.BLL
{
    public class BLL_Trainer
    {
        public static void Insertar(RutinaCliente rutinaCliente)
        {
            DAL_Trainer.Insertar(rutinaCliente);
        }

        public static List<RutinaCliente> SeleccionarTodos()
        {
            List<RutinaCliente> lista = new List<RutinaCliente>();

            DataSet ds = DAL_Trainer.SeleccionarTodos();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                RutinaCliente trainer = new RutinaCliente();
                trainer.idCliente = Convert.ToInt32(row["id"]);
                trainer.nombreCli = row["nombre"].ToString();
                trainer.dia = row["dia"].ToString();

                if (Convert.ToInt32(row["idHipertrofia"]) == 0)
                {
                    trainer.nombreEjer = row["nombreC"].ToString();
                    trainer.idCardio = Convert.ToInt32(row["idCardio"]);
                    trainer.idHipertrofia = 0;
                }
                else
                {
                    trainer.nombreEjer = row["nombreH"].ToString();
                    trainer.idHipertrofia = Convert.ToInt32(row["idHipertrofia"]);
                    trainer.idCardio = 0;
                }

                lista.Add(trainer);

            }

            return lista;
        }

        public static void Borrar(RutinaCliente rutinaCliente)
        {
            DAL_Trainer.Borrar(rutinaCliente);
        }
    }
}
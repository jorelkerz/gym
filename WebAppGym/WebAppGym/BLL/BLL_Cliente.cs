﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using WebAppGym.Clases;
using WebAppGym.DAL;

namespace WebAppGym.BLL
{
    public class BLL_Cliente
    {
        public static void Insertar(Cliente cliente)
        {
            DAL_Cliente.Insertar(cliente);
        }


        public static void ModificarClienteContrasena(int id, String contrasena)
        {
            DAL_Cliente.ModificarClienteContrasena(id, contrasena);
        }


        public static List<Cliente> SeleccionarTodos()
        {
            List<Cliente> lista = new List<Cliente>();

            DataSet ds = DAL_Cliente.SeleccionarTodos();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Cliente cliente = new Cliente();
                cliente.id = Convert.ToInt32(row["id"]);
                cliente.nombre = row["nombre"].ToString();
                cliente.fechaNacimiento = Convert.ToDateTime(row["fechaNacimiento"]);
                cliente.genero = row["genero"].ToString();
                cliente.contrasena = row["contrasena"].ToString();
                cliente.telefono = Convert.ToInt32(row["telefono"]);
                cliente.email = row["email"].ToString();
                cliente.descripcionDieta = row["descripcionDieta"].ToString();



                lista.Add(cliente);

            }

            return lista;
        }

        public static Cliente obtieneClienteporID(int id)
        {
            Cliente cli = new Cliente();

            DataSet ds = DAL_Cliente.SeleccionarClientePorID(id);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Cliente cliente = new Cliente();
                cliente.id = Convert.ToInt32(row["id"]);
                cliente.nombre = row["nombre"].ToString();
                cliente.fechaNacimiento = Convert.ToDateTime(row["fechaNacimiento"]);
                cliente.genero = row["genero"].ToString();
                cliente.contrasena = row["contrasena"].ToString();
                cliente.telefono = Convert.ToInt32(row["telefono"]);
                cliente.email = row["email"].ToString();
                cliente.descripcionDieta = row["descripcionDieta"].ToString();



                cli = cliente;

            }

            return cli;
        }

        public static void Modificar(Cliente cliente)
        {
            DAL_Cliente.Modificar(cliente);
        }

        public static void Eliminar(int id)
        {
            DAL_Cliente.Eliminar(id);
        }



        public static Medicion ObtenerMedicionporIDCliente(int idCliente)
        {

            Medicion medicion = new Medicion();
            DataSet ds = DAL_Medicion.SeleccionarMedicionPorIDCliente(idCliente);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Medicion m = new Medicion();
                m.id = Convert.ToInt32(row["id"]);
                m.fechaRegistro = Convert.ToDateTime(row["fechaRegistro"]);
                m.peso = Convert.ToDouble(row["peso"]);
                m.ritmoCardiaco = Convert.ToDouble(row["ritmoCardiaco"]);
                m.brazoRelajado = Convert.ToDouble(row["brazoRelajado"]);
                m.brazoTension = Convert.ToDouble(row["brazoTension"]);
                m.antebrazo = Convert.ToDouble(row["antebrazo"]);
                m.muslo = Convert.ToDouble(row["muslo"]);
                m.pechoNomal = Convert.ToDouble(row["pechoNomal"]);
                m.pechoHinchado = Convert.ToDouble(row["pechoHinchado"]);
                m.gemelos = Convert.ToDouble(row["gemelos"]);
                m.cintura = Convert.ToDouble(row["cintura"]);
                m.cadera = Convert.ToDouble(row["cadera"]);
                m.porcentajeGrasa = Convert.ToDouble(row["porcentajeGrasa"]);
                m.altura = Convert.ToInt32(row["altura"]);
                m.IMC = Convert.ToDouble(row["IMC"]);

                medicion = m;
            }

            return medicion;
        }

        public static List<RutinaCliente> obtieneRutinaCliente(int idCliente)
        {
            List<RutinaCliente> lista = new List<RutinaCliente>();

            RutinaCliente rutinaCliente = new RutinaCliente();
            DataSet ds = DAL.DAL_RutinaCliente.SeleccionarRutinaClientePorIDCliente(idCliente);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                RutinaCliente r = new RutinaCliente();
                r.id = Convert.ToInt32(row["id"]);
                r.idCliente = Convert.ToInt32(row["idCliente"]);
                r.dia = row["dia"].ToString();
                r.idHipertrofia = Convert.ToInt32(row["idHipertrofia"]);
                r.idCardio = Convert.ToInt32(row["idCardio"]);

                lista.Add(r);
            }

            return lista;
        }

    }
}
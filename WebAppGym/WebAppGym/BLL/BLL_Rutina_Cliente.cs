﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using WebAppGym.Clases;

namespace WebAppGym.BLL
{
    public class BLL_Rutina_Cliente
    {
        public static List<ReporteGrafico> ReporteGrafico()
        {
            List<ReporteGrafico> _items = new List<ReporteGrafico>();
            
            RutinaCliente rutinaCliente = new RutinaCliente();
            DataSet ds = DAL.DAL_RutinaCliente.ReporteGrafico();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                ReporteGrafico org = new ReporteGrafico();
                org.cantidad = Convert.ToInt32(row["cantidad"].ToString());
                org.dia = row["dia"].ToString();

                _items.Add(org);
            }

            return _items;
        }
    }
}
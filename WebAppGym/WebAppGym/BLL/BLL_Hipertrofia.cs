﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using WebAppGym.Clases;
using WebAppGym.DAL;

namespace WebAppGym.BLL
{
    public class BLL_Hipertrofia
    {

        public static void Insertar(Hipertrofia hipertrofia)
        {
            DAL_Hipertrofia.Insertar(hipertrofia);
        }

        public static List<Hipertrofia> SeleccionarTodos()
        {
            List<Hipertrofia> lista = new List<Hipertrofia>();

            DataSet ds = DAL_Hipertrofia.SeleccionarTodos();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Hipertrofia hipertrofia = new Hipertrofia();
                hipertrofia.id = Convert.ToInt32(row["id"]);
                hipertrofia.nombre = row["nombre"].ToString();
                hipertrofia.ejercicio = row["ejercicio"].ToString();
                hipertrofia.url = row["url"].ToString();
                hipertrofia.serie = row["series"].ToString();
                hipertrofia.repeticion = row["repeticiones"].ToString();
                hipertrofia.descanso = row["descanso"].ToString();
                hipertrofia.modo = row["modo"].ToString();
                hipertrofia.calorias = row["calorias"].ToString();
          

                    lista.Add(hipertrofia);
                
            }

            return lista;
        }

        public static void Modificar(Hipertrofia hipertrofia)
        {
            DAL_Hipertrofia.Modificar(hipertrofia);
        }

        public static void Eliminar(int id)
        {
            DAL_Hipertrofia.Eliminar(id);
        }




    }//FIN CLASS
}
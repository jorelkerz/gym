﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using WebAppGym.Clases;
using WebAppGym.DAL;

namespace WebAppGym.BLL
{
    public class BLL_Cardio
    {

        public static void Insertar(Cardio cardio)
        {
            DAL_Cardio.Insertar(cardio);
        }

        public static List<Cardio> SeleccionarTodos()
        {
            List<Cardio> lista = new List<Cardio>();

            DataSet ds = DAL_Cardio.SeleccionarTodos();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Cardio cardio = new Cardio();
                cardio.id = Convert.ToInt32(row["id"]);
                cardio.nombre = row["nombre"].ToString();
                cardio.ejercicio = row["ejercicio"].ToString();
                cardio.url = row["url"].ToString();
                cardio.tiempo = row["tiempo"].ToString();
                cardio.descanso = row["descanso"].ToString();
                cardio.calorias = row["calorias"].ToString();


                lista.Add(cardio);

            }

            return lista;
        }

        public static void Modificar(Cardio cardio)
        {
            DAL_Cardio.Modificar(cardio);
        }

        public static void Eliminar(int id)
        {
            DAL_Cardio.Eliminar(id);
        }
    }
}
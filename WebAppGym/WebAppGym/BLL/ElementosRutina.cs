﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAppGym.Clases;

namespace WebAppGym.BLL
{
    public class ElementosRutina
    {




        public List<urlRutinas> cargaURL()
        {
            List<urlRutinas> lista = new List<urlRutinas>();

            //Brazos
            lista.Add(new urlRutinas("Curl de Biceps con Mancuernas","http://localhost:49585/Vistas/Rutinas/rutina01Brazos.aspx"));
            lista.Add(new urlRutinas("Flexion alterna de mancuerna con el codo", "http://localhost:49585/Vistas/Rutinas/rutina02Brazos.aspx"));
            lista.Add(new urlRutinas("Flexión de codos con mancuerna, banco inclinado", "http://localhost:49585/Vistas/Rutinas/rutina03Brazos.aspx"));
            lista.Add(new urlRutinas("Flexión de codos con mancuerna corta", "http://localhost:49585/Vistas/Rutinas/rutina04Brazos.aspx"));
            lista.Add(new urlRutinas("Flexión de los codos con polea baja", "http://localhost:49585/Vistas/Rutinas/rutina05Brazos.aspx"));
            //Hombros
            lista.Add(new urlRutinas("Press tras nuca con barra", "http://localhost:49585/Vistas/Rutinas/rutina13Hombros.aspx"));
            lista.Add(new urlRutinas("Press frontal con barra", "http://localhost:49585/Vistas/Rutinas/rutina14Hombros.aspx"));
            lista.Add(new urlRutinas("Press sentado con mancuernas", "http://localhost:49585/Vistas/Rutinas/rutina15Hombros.aspx"));
            lista.Add(new urlRutinas("Elevaciones laterales, tronco inclinado hacia adelante", "http://localhost:49585/Vistas/Rutinas/rutina16Hombros.aspx"));
            lista.Add(new urlRutinas("Elevaciones laterales de los brazos con mancuernas", "http://localhost:49585/Vistas/Rutinas/rutina17Hombros.aspx"));
            //Pectorales
            lista.Add(new urlRutinas("Press de banco inclinado", "http://localhost:49585/Vistas/Rutinas/rutina18Pectorales.aspx"));
            lista.Add(new urlRutinas("Press de banco plano", "http://localhost:49585/Vistas/Rutinas/rutina19Pectorales.aspx"));
            lista.Add(new urlRutinas("Press de banco inclinado, atras", "http://localhost:49585/Vistas/Rutinas/rutina20Pectorales.aspx"));
            lista.Add(new urlRutinas("Aperturas con mancuernas en banco plano", "http://localhost:49585/Vistas/Rutinas/rutina21Pectorales.aspx"));
            lista.Add(new urlRutinas("Pull-Over con barra en banco plano", "http://localhost:49585/Vistas/Rutinas/rutina22Pectorales.aspx"));
            //Espalda
            lista.Add(new urlRutinas("Tracción o dominadas en barra fija", "http://localhost:49585/Vistas/Rutinas/rutina23Espalda.aspx"));
            lista.Add(new urlRutinas("Tracción pecho en polea alta", "http://localhost:49585/Vistas/Rutinas/rutina24Espalda.aspx"));
            lista.Add(new urlRutinas("Tracción tras nuca en polea alta", "http://localhost:49585/Vistas/Rutinas/rutina25Espalda.aspx"));
            lista.Add(new urlRutinas("Remo en polea baja, manos en semipronación", "http://localhost:49585/Vistas/Rutinas/rutina26Espalda.aspx"));
            lista.Add(new urlRutinas("Remo horizontal a una mano con mancuernas", "http://localhost:49585/Vistas/Rutinas/rutina27Espalda.aspx"));
            //Piernas
            lista.Add(new urlRutinas("El squat o sentadillas", "http://localhost:49585/Vistas/Rutinas/rutina28Piernas.aspx"));
            lista.Add(new urlRutinas("Press de piernas inclinado", "http://localhost:49585/Vistas/Rutinas/rutina29Piernas.aspx"));
            lista.Add(new urlRutinas("Extensión de rodillas en máquina", "http://localhost:49585/Vistas/Rutinas/rutina30Piernas.aspx"));
            lista.Add(new urlRutinas("Isquiotibiales, en máquina o Leg Curl", "http://localhost:49585/Vistas/Rutinas/rutina31Piernas.aspx"));
            lista.Add(new urlRutinas("Extensión de tobillos en la máquina", "http://localhost:49585/Vistas/Rutinas/rutina32Piernas.aspx"));
            //Abdomen
            lista.Add(new urlRutinas("Encogimientos Abdominales o Crunch", "http://localhost:49585/Vistas/Rutinas/rutina33Abdomen.aspx"));
            lista.Add(new urlRutinas("Elevaciones de tronco en el suelo", "http://localhost:49585/Vistas/Rutinas/rutina34Abdomen.aspx"));
            lista.Add(new urlRutinas("Elevaciones de piernas en plancha inclinada", "http://localhost:49585/Vistas/Rutinas/rutina35Abdomen.aspx"));
            lista.Add(new urlRutinas("Elevaciones de rodillas paralelas", "http://localhost:49585/Vistas/Rutinas/rutina36Abdomen.aspx"));
            lista.Add(new urlRutinas("Flexión lateral del tronco con mancuerna", "http://localhost:49585/Vistas/Rutinas/rutina37Abdomen.aspx"));


            return lista;
        }

        public List<String> cargaSeries()
        {
            List<String> lista = new List<String>();
            lista.Add("Series");
            for (int i = 1; i < 26; i++)
            {
                lista.Add(Convert.ToString(i));
            }
            return lista;
        }

        public List<String> cargaRepeticiones()
        {
            List<String> lista = new List<String>();
            lista.Add("Repeticiones");
            for (int i = 1; i < 26; i++)
            {
                lista.Add(Convert.ToString(i));
            }
            return lista;
        }

        public List<String> cargaDescansos()
        {
            List<String> lista = new List<String>();
            lista.Add("Descansos");
            for (int i = 20; i < 120; i += 10)
            {
                lista.Add(Convert.ToString(i) + " segundos");
            }
            return lista;
        }

        public List<String> cargaModos()
        {
            List<String> lista = new List<String>();

            lista.Add("Modo");
            lista.Add("Lento");
            lista.Add("Normal");
            lista.Add("Rapido");

            return lista;
        }

        public List<urlRutinas> cargaURLCardio()
        {
            List<urlRutinas> lista = new List<urlRutinas>();

            lista.Add(new urlRutinas("Salto de Cuerda", "http://localhost:49585/Vistas/Rutinas/rutinaCuerda.aspx"));
            lista.Add(new urlRutinas("Spinnin", "http://localhost:49585/Vistas/Rutinas/rutinaSpinning.aspx"));
         
            return lista;
        }

        public List<String> cargaTiempo()
        {
            List<String> lista = new List<String>();
            lista.Add("Tiempo");
            lista.Add(Convert.ToString("1") + " minuto");

            for (int i = 2; i < 120; i++)
            {
                lista.Add(Convert.ToString(i) + " minutos");
            }
            return lista;
        }

        public String calculoCalorias(String serie, String repeticiones) {

            int ser = Convert.ToInt32(serie);
            int rep = Convert.ToInt32(repeticiones);
            int resultado = (ser * rep) * 10;
            return Convert.ToString(resultado);
                    
        }

        public String calculoCaloriasCardio(String minutos)
        {
            int resultado = 0;
            int contador = 1;
            foreach (String item in cargaTiempo())
            {
                if (minutos == item) {
                    resultado = contador * 100;
                }
                contador++;
            }
            
            return Convert.ToString(resultado);

        }


    }//fin class
}
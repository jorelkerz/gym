﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using WebAppGym.Clases;
using WebAppGym.DAL;

namespace WebAppGym.BLL
{
    public class BLL_Medicion
    {

        public static void Eliminar(int id)
        {
            DAL_Medicion.Eliminar(id);
        }

        public static void Insertar(Medicion oMedicion)
        {
            DAL_Medicion.Insertar(oMedicion);
        }

        public static List<Medicion> ObtenerMedicionReporte(int idCliente)
        {

            List<Medicion> lista = new List<Medicion>();
            DataSet ds = DAL_Medicion.SeleccionarMedicionReporte(idCliente);

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Medicion m = new Medicion();
                m.fechaRegistro = Convert.ToDateTime(row["fechaRegistro"]);
                m.peso = Convert.ToDouble(row["peso"]);
                m.ritmoCardiaco = Convert.ToDouble(row["ritmoCardiaco"]);
                m.brazoRelajado = Convert.ToDouble(row["brazoRelajado"]);
                m.brazoTension = Convert.ToDouble(row["brazoTension"]);
                m.antebrazo = Convert.ToDouble(row["antebrazo"]);
                m.muslo = Convert.ToDouble(row["muslo"]);
                m.pechoNomal = Convert.ToDouble(row["pechoNomal"]);
                m.pechoHinchado = Convert.ToDouble(row["pechoHinchado"]);
                m.gemelos = Convert.ToDouble(row["gemelos"]);
                m.cintura = Convert.ToDouble(row["cintura"]);
                m.cadera = Convert.ToDouble(row["cadera"]);
                m.porcentajeGrasa = Convert.ToDouble(row["porcentajeGrasa"]);
                m.altura = Convert.ToInt32(row["altura"]);
                m.IMC = Convert.ToDouble(row["IMC"]);

                lista.Add(m);
            }

            return lista;
        }

    }
}
﻿<?xml version="1.0" encoding="utf-8"?>
<Project ToolsVersion="4.0" DefaultTargets="Build" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
  <Import Project="$(MSBuildExtensionsPath)\$(MSBuildToolsVersion)\Microsoft.Common.props" Condition="Exists('$(MSBuildExtensionsPath)\$(MSBuildToolsVersion)\Microsoft.Common.props')" />
  <PropertyGroup>
    <Configuration Condition=" '$(Configuration)' == '' ">Debug</Configuration>
    <Platform Condition=" '$(Platform)' == '' ">AnyCPU</Platform>
    <ProductVersion>
    </ProductVersion>
    <SchemaVersion>2.0</SchemaVersion>
    <ProjectGuid>{D6493307-C2F1-4865-856F-764F2F0022B8}</ProjectGuid>
    <ProjectTypeGuids>{349c5851-65df-11da-9384-00065b846f21};{fae04ec0-301f-11d3-bf4b-00c04f79efbc}</ProjectTypeGuids>
    <OutputType>Library</OutputType>
    <AppDesignerFolder>Properties</AppDesignerFolder>
    <RootNamespace>WebAppGym</RootNamespace>
    <AssemblyName>WebAppGym</AssemblyName>
    <TargetFrameworkVersion>v4.5</TargetFrameworkVersion>
    <UseIISExpress>true</UseIISExpress>
    <IISExpressSSLPort />
    <IISExpressAnonymousAuthentication />
    <IISExpressWindowsAuthentication />
    <IISExpressUseClassicPipelineMode />
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' ">
    <DebugSymbols>true</DebugSymbols>
    <DebugType>full</DebugType>
    <Optimize>false</Optimize>
    <OutputPath>bin\</OutputPath>
    <DefineConstants>DEBUG;TRACE</DefineConstants>
    <ErrorReport>prompt</ErrorReport>
    <WarningLevel>4</WarningLevel>
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Release|AnyCPU' ">
    <DebugType>pdbonly</DebugType>
    <Optimize>true</Optimize>
    <OutputPath>bin\</OutputPath>
    <DefineConstants>TRACE</DefineConstants>
    <ErrorReport>prompt</ErrorReport>
    <WarningLevel>4</WarningLevel>
  </PropertyGroup>
  <ItemGroup>
    <Reference Include="AjaxControlToolkit, Version=4.5.7.1213, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e, processorArchitecture=MSIL">
      <SpecificVersion>False</SpecificVersion>
      <HintPath>..\..\..\..\..\Dropbox\Programacion\Progra6\Leccion5\AjaxControlToolkit.Binary.NET45\AjaxControlToolkit.dll</HintPath>
    </Reference>
    <Reference Include="Microsoft.CSharp" />
    <Reference Include="System.Web.DynamicData" />
    <Reference Include="System.Web.Entity" />
    <Reference Include="System.Web.ApplicationServices" />
    <Reference Include="System.ComponentModel.DataAnnotations" />
    <Reference Include="System" />
    <Reference Include="System.Data" />
    <Reference Include="System.Core" />
    <Reference Include="System.Data.DataSetExtensions" />
    <Reference Include="System.Web.Extensions" />
    <Reference Include="System.Xml.Linq" />
    <Reference Include="System.Drawing" />
    <Reference Include="System.Web" />
    <Reference Include="System.Xml" />
    <Reference Include="System.Configuration" />
    <Reference Include="System.Web.Services" />
    <Reference Include="System.EnterpriseServices" />
  </ItemGroup>
  <ItemGroup>
    <Content Include="css\jlcss.css" />
    <Content Include="Vistas\Perfil\mantenimientoCardio.aspx" />
    <Content Include="Vistas\Perfil\mantenimientoCliente.aspx" />
    <Content Include="Vistas\Perfil\mantenimientoHipertrofia.aspx" />
    <Content Include="Vistas\Perfil\Trainer.aspx" />
    <Content Include="Vistas\Rutinas\images\rutina13Hombros\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina14Hombros\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina15Hombros\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina15Hombros\02.png" />
    <Content Include="Vistas\Rutinas\images\rutina16Hombros\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina17Hombros\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina18Pectorales\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina19Pectorales\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina20Pectorales\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina21Pectorales\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina22Pectorales\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina23Espalda\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina24Espalda\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina25Espalda\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina26Espalda\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina28Piernas\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina29Piernas\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina30Piernas\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina31Piernas\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina32Piernas\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina32Piernas\02.png" />
    <Content Include="Vistas\Rutinas\images\rutina32Piernas\03.png" />
    <Content Include="Vistas\Rutinas\images\rutina33Abdomen\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina33Abdomen\02.png" />
    <Content Include="Vistas\Rutinas\images\rutina33Abdomen\03.png" />
    <Content Include="Vistas\Rutinas\images\rutina34Abdomen\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina34Abdomen\02.png" />
    <Content Include="Vistas\Rutinas\images\rutina35Abdomen\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina36Abdomen\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina37Abdomen\01.png" />
    <Content Include="Vistas\Rutinas\rutina27Espalda.aspx" />
    <Content Include="Vistas\Rutinas\images\rutina27Espalda\01.png" />
    <Content Include="Vistas\Rutinas\rutina11Brazos.aspx" />
    <Content Include="Vistas\Rutinas\images\rutina01Brazos\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina01Brazos\02.png" />
    <Content Include="Vistas\Rutinas\images\rutina01Brazos\03.png" />
    <Content Include="Vistas\Rutinas\images\rutina02Brazos\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina03Brazos\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina04Brazos\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina05Brazos\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina05Brazos\02.png" />
    <Content Include="Vistas\Rutinas\images\rutina06Brazos\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina07Brazos\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina08Brazos\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina08Brazos\02.png" />
    <Content Include="Vistas\Rutinas\images\rutina09Brazos\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina10Brazos\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina11Brazos\01.png" />
    <Content Include="Vistas\Rutinas\images\rutina12Brazos\01.png" />
    <Content Include="Vistas\Rutinas\rutina01Brazos.aspx" />
    <Content Include="Vistas\Rutinas\rutina02Brazos.aspx" />
    <Content Include="Vistas\Rutinas\rutina03Brazos.aspx" />
    <Content Include="Vistas\Rutinas\rutina04Brazos.aspx" />
    <Content Include="Vistas\Rutinas\rutina05Brazos.aspx" />
    <Content Include="Vistas\Rutinas\rutina06Brazos.aspx" />
    <Content Include="Vistas\Rutinas\rutina07Brazos.aspx" />
    <Content Include="Vistas\Rutinas\rutina08Brazos.aspx" />
    <Content Include="Vistas\Rutinas\rutina09Brazos.aspx" />
    <Content Include="Vistas\Rutinas\rutina10Brazos.aspx" />
    <Content Include="Vistas\Rutinas\rutina12Brazos.aspx" />
    <Content Include="Vistas\Rutinas\rutina13Hombros.aspx" />
    <Content Include="Vistas\Rutinas\rutina14Hombros.aspx" />
    <Content Include="Vistas\Rutinas\rutina15Hombros.aspx" />
    <Content Include="Vistas\Rutinas\rutina16Hombros.aspx" />
    <Content Include="Vistas\Rutinas\rutina17Hombros.aspx" />
    <Content Include="Vistas\Rutinas\rutina18Pectorales.aspx" />
    <Content Include="Vistas\Rutinas\rutina19Pectoreales.aspx" />
    <Content Include="Vistas\Rutinas\rutina20Pectorales.aspx" />
    <Content Include="Vistas\Rutinas\rutina22Pectorales.aspx" />
    <Content Include="Vistas\Rutinas\rutina23Espalda.aspx" />
    <Content Include="Vistas\Rutinas\rutina24Espalda.aspx" />
    <Content Include="Vistas\Rutinas\rutina25Espalda.aspx" />
    <Content Include="Vistas\Rutinas\rutina26Espalda.aspx" />
    <Content Include="Vistas\Rutinas\rutina28Piernas.aspx" />
    <Content Include="Vistas\Rutinas\rutina29Piernas.aspx" />
    <Content Include="Vistas\Rutinas\rutina30Piernas.aspx" />
    <Content Include="Vistas\Rutinas\rutina31Piernas.aspx" />
    <Content Include="Vistas\Rutinas\rutina32Piernas.aspx" />
    <Content Include="Vistas\Rutinas\rutina33Abdomen.aspx" />
    <Content Include="Vistas\Rutinas\rutina34Abdomen.aspx" />
    <Content Include="Vistas\Rutinas\rutina35Abdomen.aspx" />
    <Content Include="Vistas\Rutinas\rutina36Abdomne.aspx" />
    <Content Include="Vistas\Rutinas\rutina37Abdomen.aspx" />
    <Content Include="Vistas\Rutinas\rutinas21Pectorales.aspx" />
    <Content Include="Web.config" />
  </ItemGroup>
  <ItemGroup>
    <Compile Include="BLL\BLL_Hipertrofia.cs" />
    <Compile Include="BLL\ElementosRutina.cs" />
    <Compile Include="Clases\Cardio.cs" />
    <Compile Include="Clases\Cliente.cs" />
    <Compile Include="Clases\Dieta.cs" />
    <Compile Include="Clases\Empleado.cs" />
    <Compile Include="Clases\Hipertrofia.cs" />
    <Compile Include="Clases\Medicion.cs" />
    <Compile Include="Clases\Persona.cs" />
    <Compile Include="Clases\Rutina.cs" />
    <Compile Include="Clases\urlRutinas.cs" />
    <Compile Include="DAL\DAL_Hipertrofia.cs" />
    <Compile Include="Persistencia\Database.cs" />
    <Compile Include="Persistencia\DatabaseFactory.cs" />
    <Compile Include="Persistencia\Log.cs" />
    <Compile Include="Properties\AssemblyInfo.cs" />
    <Compile Include="Singleton\GestorTrainer.cs" />
    <Compile Include="Vistas\Perfil\mantenimientoCardio.aspx.cs">
      <DependentUpon>mantenimientoCardio.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Perfil\mantenimientoCardio.aspx.designer.cs">
      <DependentUpon>mantenimientoCardio.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Perfil\mantenimientoCliente.aspx.cs">
      <DependentUpon>mantenimientoCliente.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Perfil\mantenimientoCliente.aspx.designer.cs">
      <DependentUpon>mantenimientoCliente.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Perfil\mantenimientoHipertrofia.aspx.cs">
      <DependentUpon>mantenimientoHipertrofia.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Perfil\mantenimientoHipertrofia.aspx.designer.cs">
      <DependentUpon>mantenimientoHipertrofia.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Perfil\perfilTrainerMaster.Master.cs">
      <DependentUpon>perfilTrainerMaster.Master</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Perfil\perfilTrainerMaster.Master.designer.cs">
      <DependentUpon>perfilTrainerMaster.Master</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Perfil\Trainer.aspx.cs">
      <DependentUpon>Trainer.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Perfil\Trainer.aspx.designer.cs">
      <DependentUpon>Trainer.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina27Espalda.aspx.cs">
      <DependentUpon>rutina27Espalda.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina27Espalda.aspx.designer.cs">
      <DependentUpon>rutina27Espalda.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina11Brazos.aspx.cs">
      <DependentUpon>rutina11Brazos.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina11Brazos.aspx.designer.cs">
      <DependentUpon>rutina11Brazos.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina01Brazos.aspx.cs">
      <DependentUpon>rutina01Brazos.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina01Brazos.aspx.designer.cs">
      <DependentUpon>rutina01Brazos.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina02Brazos.aspx.cs">
      <DependentUpon>rutina02Brazos.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina02Brazos.aspx.designer.cs">
      <DependentUpon>rutina02Brazos.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina03Brazos.aspx.cs">
      <DependentUpon>rutina03Brazos.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina03Brazos.aspx.designer.cs">
      <DependentUpon>rutina03Brazos.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina04Brazos.aspx.cs">
      <DependentUpon>rutina04Brazos.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina04Brazos.aspx.designer.cs">
      <DependentUpon>rutina04Brazos.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina05Brazos.aspx.cs">
      <DependentUpon>rutina05Brazos.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina05Brazos.aspx.designer.cs">
      <DependentUpon>rutina05Brazos.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina06Brazos.aspx.cs">
      <DependentUpon>rutina06Brazos.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina06Brazos.aspx.designer.cs">
      <DependentUpon>rutina06Brazos.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina07Brazos.aspx.cs">
      <DependentUpon>rutina07Brazos.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina07Brazos.aspx.designer.cs">
      <DependentUpon>rutina07Brazos.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina08Brazos.aspx.cs">
      <DependentUpon>rutina08Brazos.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina08Brazos.aspx.designer.cs">
      <DependentUpon>rutina08Brazos.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina09Brazos.aspx.cs">
      <DependentUpon>rutina09Brazos.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina09Brazos.aspx.designer.cs">
      <DependentUpon>rutina09Brazos.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina10Brazos.aspx.cs">
      <DependentUpon>rutina10Brazos.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina10Brazos.aspx.designer.cs">
      <DependentUpon>rutina10Brazos.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina12Brazos.aspx.cs">
      <DependentUpon>rutina12Brazos.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina12Brazos.aspx.designer.cs">
      <DependentUpon>rutina12Brazos.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina13Hombros.aspx.cs">
      <DependentUpon>rutina13Hombros.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina13Hombros.aspx.designer.cs">
      <DependentUpon>rutina13Hombros.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina14Hombros.aspx.cs">
      <DependentUpon>rutina14Hombros.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina14Hombros.aspx.designer.cs">
      <DependentUpon>rutina14Hombros.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina15Hombros.aspx.cs">
      <DependentUpon>rutina15Hombros.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina15Hombros.aspx.designer.cs">
      <DependentUpon>rutina15Hombros.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina16Hombros.aspx.cs">
      <DependentUpon>rutina16Hombros.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina16Hombros.aspx.designer.cs">
      <DependentUpon>rutina16Hombros.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina17Hombros.aspx.cs">
      <DependentUpon>rutina17Hombros.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina17Hombros.aspx.designer.cs">
      <DependentUpon>rutina17Hombros.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina18Pectorales.aspx.cs">
      <DependentUpon>rutina18Pectorales.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina18Pectorales.aspx.designer.cs">
      <DependentUpon>rutina18Pectorales.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina19Pectoreales.aspx.cs">
      <DependentUpon>rutina19Pectoreales.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina19Pectoreales.aspx.designer.cs">
      <DependentUpon>rutina19Pectoreales.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina20Pectorales.aspx.cs">
      <DependentUpon>rutina20Pectorales.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina20Pectorales.aspx.designer.cs">
      <DependentUpon>rutina20Pectorales.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina22Pectorales.aspx.cs">
      <DependentUpon>rutina22Pectorales.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina22Pectorales.aspx.designer.cs">
      <DependentUpon>rutina22Pectorales.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina23Espalda.aspx.cs">
      <DependentUpon>rutina23Espalda.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina23Espalda.aspx.designer.cs">
      <DependentUpon>rutina23Espalda.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina24Espalda.aspx.cs">
      <DependentUpon>rutina24Espalda.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina24Espalda.aspx.designer.cs">
      <DependentUpon>rutina24Espalda.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina25Espalda.aspx.cs">
      <DependentUpon>rutina25Espalda.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina25Espalda.aspx.designer.cs">
      <DependentUpon>rutina25Espalda.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina26Espalda.aspx.cs">
      <DependentUpon>rutina26Espalda.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina26Espalda.aspx.designer.cs">
      <DependentUpon>rutina26Espalda.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina28Piernas.aspx.cs">
      <DependentUpon>rutina28Piernas.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina28Piernas.aspx.designer.cs">
      <DependentUpon>rutina28Piernas.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina29Piernas.aspx.cs">
      <DependentUpon>rutina29Piernas.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina29Piernas.aspx.designer.cs">
      <DependentUpon>rutina29Piernas.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina30Piernas.aspx.cs">
      <DependentUpon>rutina30Piernas.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina30Piernas.aspx.designer.cs">
      <DependentUpon>rutina30Piernas.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina31Piernas.aspx.cs">
      <DependentUpon>rutina31Piernas.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina31Piernas.aspx.designer.cs">
      <DependentUpon>rutina31Piernas.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina32Piernas.aspx.cs">
      <DependentUpon>rutina32Piernas.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina32Piernas.aspx.designer.cs">
      <DependentUpon>rutina32Piernas.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina33Abdomen.aspx.cs">
      <DependentUpon>rutina33Abdomen.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina33Abdomen.aspx.designer.cs">
      <DependentUpon>rutina33Abdomen.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina34Abdomen.aspx.cs">
      <DependentUpon>rutina34Abdomen.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina34Abdomen.aspx.designer.cs">
      <DependentUpon>rutina34Abdomen.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina35Abdomen.aspx.cs">
      <DependentUpon>rutina35Abdomen.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina35Abdomen.aspx.designer.cs">
      <DependentUpon>rutina35Abdomen.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina36Abdomne.aspx.cs">
      <DependentUpon>rutina36Abdomne.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina36Abdomne.aspx.designer.cs">
      <DependentUpon>rutina36Abdomne.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina37Abdomen.aspx.cs">
      <DependentUpon>rutina37Abdomen.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutina37Abdomen.aspx.designer.cs">
      <DependentUpon>rutina37Abdomen.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutinas21Pectorales.aspx.cs">
      <DependentUpon>rutinas21Pectorales.aspx</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutinas21Pectorales.aspx.designer.cs">
      <DependentUpon>rutinas21Pectorales.aspx</DependentUpon>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutinasMaster.Master.cs">
      <DependentUpon>rutinasMaster.Master</DependentUpon>
      <SubType>ASPXCodeBehind</SubType>
    </Compile>
    <Compile Include="Vistas\Rutinas\rutinasMaster.Master.designer.cs">
      <DependentUpon>rutinasMaster.Master</DependentUpon>
    </Compile>
  </ItemGroup>
  <ItemGroup>
    <Content Include="Clases\ClassDiagram1.cd" />
    <Content Include="Vistas\Rutinas\rutinasMaster.Master" />
    <Content Include="Vistas\Perfil\perfilTrainerMaster.Master" />
    <None Include="Web.Debug.config">
      <DependentUpon>Web.config</DependentUpon>
    </None>
    <None Include="Web.Release.config">
      <DependentUpon>Web.config</DependentUpon>
    </None>
  </ItemGroup>
  <ItemGroup>
    <Folder Include="Vistas\Perfil\images\" />
  </ItemGroup>
  <PropertyGroup>
    <VisualStudioVersion Condition="'$(VisualStudioVersion)' == ''">10.0</VisualStudioVersion>
    <VSToolsPath Condition="'$(VSToolsPath)' == ''">$(MSBuildExtensionsPath32)\Microsoft\VisualStudio\v$(VisualStudioVersion)</VSToolsPath>
  </PropertyGroup>
  <Import Project="$(MSBuildBinPath)\Microsoft.CSharp.targets" />
  <Import Project="$(VSToolsPath)\WebApplications\Microsoft.WebApplication.targets" Condition="'$(VSToolsPath)' != ''" />
  <Import Project="$(MSBuildExtensionsPath32)\Microsoft\VisualStudio\v10.0\WebApplications\Microsoft.WebApplication.targets" Condition="false" />
  <ProjectExtensions>
    <VisualStudio>
      <FlavorProperties GUID="{349c5851-65df-11da-9384-00065b846f21}">
        <WebProjectProperties>
          <UseIIS>True</UseIIS>
          <AutoAssignPort>True</AutoAssignPort>
          <DevelopmentServerPort>0</DevelopmentServerPort>
          <DevelopmentServerVPath>/</DevelopmentServerVPath>
          <IISUrl>http://localhost:49585/</IISUrl>
          <NTLMAuthentication>False</NTLMAuthentication>
          <UseCustomServer>False</UseCustomServer>
          <CustomServerUrl>
          </CustomServerUrl>
          <SaveServerSettingsInUserFile>False</SaveServerSettingsInUserFile>
        </WebProjectProperties>
      </FlavorProperties>
    </VisualStudio>
  </ProjectExtensions>
  <!-- To modify your build process, add your task inside one of the targets below and uncomment it. 
       Other similar extension points exist, see Microsoft.Common.targets.
  <Target Name="BeforeBuild">
  </Target>
  <Target Name="AfterBuild">
  </Target>
  -->
</Project>
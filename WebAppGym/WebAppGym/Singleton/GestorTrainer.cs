﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAppGym.BLL;
using WebAppGym.Clases;

namespace WebAppGym.Singleton
{
    public class GestorTrainer
    {
        private static GestorTrainer Instancia;
        public static int idSeccion { get; set; }

        private GestorTrainer()
        {

        }

        public static GestorTrainer GetInstacia()
        {

            if (Instancia == null)
            {
                Instancia = new GestorTrainer();
            }

            return Instancia;
        }

        public List<urlRutinas> urlRutinas()
        {
            ElementosRutina lista = new ElementosRutina();
            return lista.cargaURL();
        }

        public List<String> cargarSeries()
        {
            ElementosRutina lista = new ElementosRutina();
            return lista.cargaSeries();
        }

        public List<String> cargarRepeticiones()
        {
            ElementosRutina lista = new ElementosRutina();
            return lista.cargaRepeticiones();
        }

        public List<String> cargarDescansos()
        {
            ElementosRutina lista = new ElementosRutina();
            return lista.cargaDescansos();
        }

        public List<String> cargarModo()
        {
            ElementosRutina lista = new ElementosRutina();
            return lista.cargaModos();
        }


        //cardios..............................

        public List<urlRutinas> cargaURLCardio()
        {
            ElementosRutina lista = new ElementosRutina();
            return lista.cargaURLCardio();
        }

        public List<String> cargaTiempo()
        {
            ElementosRutina lista = new ElementosRutina();
            return lista.cargaTiempo();
        }

        public static void InsertarCardio(Cardio cardio)
        {
            BLL_Cardio.Insertar(cardio);
        }

        public static List<Cardio> ListaCardio()
        {
            return BLL_Cardio.SeleccionarTodos();
        }

        public static void ModificarCardio(Cardio cardio)
        {
            BLL_Cardio.Modificar(cardio);
        }

        public static void EliminarCardio(int id)
        {
            BLL_Cardio.Eliminar(id);
        }

        public static String calculoCaloriasCardio(String minutos)
        {

            ElementosRutina calorias = new ElementosRutina();

            return calorias.calculoCaloriasCardio(minutos);
        }


        //BLL_Hipertrofia........................

        public static void Insertar(Hipertrofia hipertrofia)
        {
            BLL_Hipertrofia.Insertar(hipertrofia);
        }

        public static void Modificar(Hipertrofia hipertrofia)
        {
            BLL_Hipertrofia.Modificar(hipertrofia);
        }

        public static void Eliminar(int id)
        {
            BLL_Hipertrofia.Eliminar(id);
        }

        public static List<Hipertrofia> ListaHipertrofia()
        {
            return BLL_Hipertrofia.SeleccionarTodos();
        }


        public static String calculoCalorias(String serie, String repeticiones)
        {

            ElementosRutina calorias = new ElementosRutina();

            return calorias.calculoCalorias(serie, repeticiones);
        }

        //BLL_Cliente........................

        public static void InsertarCliente(Cliente cliente)
        {
            BLL_Cliente.Insertar(cliente);
        }

        public static void ModificarClienteContrasena(int id, String contrasena)
        {
            BLL_Cliente.ModificarClienteContrasena(id, contrasena);
        }

        public static void ModificarCliente(Cliente cliente)
        {
            BLL_Cliente.Modificar(cliente);
        }

        public static void EliminarCliente(int id)
        {
            BLL_Cliente.Eliminar(id);
        }

        public static List<Cliente> ListaClientes()
        {
            return BLL_Cliente.SeleccionarTodos();
        }

        public static Cliente ObtenerClienteporID(int id)
        {
            return BLL_Cliente.obtieneClienteporID(id);
        }

        public static void InsertarMedicionporIDCliente(Medicion oMedicion)
        {
            BLL_Medicion.Insertar(oMedicion);
        }

        public static Medicion ObtenerMedicionporIDCliente(int idCliente)
        {
            return BLL_Cliente.ObtenerMedicionporIDCliente(idCliente);
        }

        public static List<RutinaCliente> obtieneRutinaCliente(int idCliente)
        {
            return BLL_Cliente.obtieneRutinaCliente(idCliente);
        }

        public static void EliminarMediciones(int id)
        {
            BLL_Medicion.Eliminar(id);
        }

        public static List<Clases.urlRutinas> c { get; set; }

        //BLL_Trainer

        public static void InsertarTrainer(RutinaCliente rutinaCliente)
        {
            BLL_Trainer.Insertar(rutinaCliente);
        }

        public static List<RutinaCliente> rutinasTrainer()
        {
            return BLL_Trainer.SeleccionarTodos();
        }

        public static void BorrarTrainer(RutinaCliente rutinaCliente)
        {
            BLL_Trainer.Borrar(rutinaCliente);
        }
    }//fin class
}